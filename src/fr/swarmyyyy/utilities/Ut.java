package fr.swarmyyyy.utilities;

import java.util.LinkedHashSet;

public final class Ut {
	
	public static <T> LinkedHashSet<T> arrayAsLinkedHashSet(T[] array) {
		LinkedHashSet<T> list = new LinkedHashSet<T>();
		for (T member : array) {
			list.add(member);
		}
		return list;
	}
	
}
