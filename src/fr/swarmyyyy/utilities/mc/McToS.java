package fr.swarmyyyy.utilities.mc;

import java.util.Collection;
import java.util.Hashtable;
import java.util.LinkedHashSet;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeModifier;
import org.bukkit.attribute.AttributeModifier.Operation;
import org.bukkit.craftbukkit.libs.org.apache.commons.lang3.tuple.Pair;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.potion.PotionEffectType;

public final class McToS {

	// TODO Implements
	private static Hashtable<Material, String> matToS = new Hashtable<Material, String>() {
		private static final long serialVersionUID = -1780062620494190267L;
		{
			put(Material.BOW, "Bow");
			put(Material.ARROW, "Arrow");
			put(Material.WOODEN_SWORD, "Wooden Sword");
		}
	};

	private static Hashtable<Operation, String> opToS = new Hashtable<Operation, String>() {
		private static final long serialVersionUID = -386150688063867236L;
		{
			put(Operation.ADD_NUMBER, "+");
			put(Operation.ADD_SCALAR, "+");
			put(Operation.MULTIPLY_SCALAR_1, "*");
		}
	};

	private static Hashtable<Enchantment, String> enchToS = new Hashtable<Enchantment, String>() {
		private static final long serialVersionUID = -2720208547771639961L;
		{
			put(Enchantment.ARROW_DAMAGE, "Power");
			put(Enchantment.ARROW_FIRE, "Flame");
			put(Enchantment.ARROW_INFINITE, "Infinite");
			put(Enchantment.ARROW_KNOCKBACK, "Punch");
			put(Enchantment.BINDING_CURSE, "Curse of Binding");
			put(Enchantment.CHANNELING, "Channeling");
			put(Enchantment.DAMAGE_ALL, "Sharpness");
			put(Enchantment.DAMAGE_ARTHROPODS, "Bane of the Arthropods");
			put(Enchantment.DAMAGE_UNDEAD, "Smite");
			put(Enchantment.DEPTH_STRIDER, "Depth Strider");
			put(Enchantment.DIG_SPEED, "Efficiency");
			put(Enchantment.DURABILITY, "Unbreaking");
			put(Enchantment.FIRE_ASPECT, "Fire Aspect");
			put(Enchantment.FROST_WALKER, "Frost Walker");
			put(Enchantment.IMPALING, "Impaling");
			put(Enchantment.KNOCKBACK, "Knockback");
			put(Enchantment.LOOT_BONUS_BLOCKS, "Fortune");
			put(Enchantment.LOOT_BONUS_MOBS, "Looting");
			put(Enchantment.LOYALTY, "Loyalty");
			put(Enchantment.LUCK, "Luck of the Sea");
			put(Enchantment.LURE, "Lure");
			put(Enchantment.MENDING, "Mending");
			put(Enchantment.MULTISHOT, "Multishot");
			put(Enchantment.OXYGEN, "Respiration");
			put(Enchantment.PIERCING, "Piercing");
			put(Enchantment.PROTECTION_ENVIRONMENTAL, "Protection");
			put(Enchantment.PROTECTION_EXPLOSIONS, "Blast Protection");
			put(Enchantment.PROTECTION_FALL, "Feather Falling");
			put(Enchantment.PROTECTION_FIRE, "Fire Protection");
			put(Enchantment.PROTECTION_PROJECTILE, "Projectile Protection");
			put(Enchantment.QUICK_CHARGE, "Quick Charge");
			put(Enchantment.RIPTIDE, "Riptide");
			put(Enchantment.SILK_TOUCH, "Silk Touch");
			put(Enchantment.SWEEPING_EDGE, "Sweeping Edge");
			put(Enchantment.THORNS, "Thorns");
			put(Enchantment.VANISHING_CURSE, "Curse of Vanishing");
			put(Enchantment.WATER_WORKER, "Aqua Affinity");
		}
	};

	private static Hashtable<Attribute, String> attrToS = new Hashtable<Attribute, String>() {
		private static final long serialVersionUID = -8790177126226737183L;
		{
			put(Attribute.GENERIC_ARMOR, "Armor");
			put(Attribute.GENERIC_ARMOR_TOUGHNESS, "Toughness");
			put(Attribute.GENERIC_ATTACK_DAMAGE, "Attack Damage");
			put(Attribute.GENERIC_ATTACK_SPEED, "Attack Speed");
			put(Attribute.GENERIC_FLYING_SPEED, "Flying Speed");
			put(Attribute.GENERIC_FOLLOW_RANGE, "Follow Range");
			put(Attribute.GENERIC_KNOCKBACK_RESISTANCE, "Knockback Resistance");
			put(Attribute.GENERIC_LUCK, "Luck");
			put(Attribute.GENERIC_MAX_HEALTH, "Max Health");
			put(Attribute.GENERIC_MOVEMENT_SPEED, "Speed");
			put(Attribute.HORSE_JUMP_STRENGTH, "Horse Jump Strength");
			put(Attribute.ZOMBIE_SPAWN_REINFORCEMENTS, "Zombie Spawn Reinforcements");
		}
	};

	private static String playerAttrToString(Pair<Attribute, Double> playerAttr) {
		return String.format("%s : %s", playerAttr.getLeft(), playerAttr.getRight());
	}

	public static String playerAttrsToString(LinkedHashSet<Pair<Attribute, Double>> playerAttrs) {
		String rtn = "";
		for (Pair<Attribute, Double> playerAttr : playerAttrs) {
			rtn = String.format("%s%s;", rtn, playerAttrToString(playerAttr));
		}
		return rtn;
	}

	private static String attrModifToString(Collection<AttributeModifier> attrModifs) {
		String rtn = "";
		for (AttributeModifier attrModif : attrModifs) {
			rtn = String.format("%s%s%s", rtn,
					attrModif.getAmount() < 0 && (attrModif.getOperation() == Operation.ADD_NUMBER
							|| attrModif.getOperation() == Operation.ADD_SCALAR) ? ""
									: opToS.get(attrModif.getOperation()),
					attrModif.getAmount());
		}
		return rtn;
	}

	@SuppressWarnings("unchecked")
	private static String attrToString(Map<Attribute, Collection<AttributeModifier>> attrs) {
		String rtn = "";
		int i = 0;
		for (Attribute attr : attrs.keySet()) {
			String attrToS = attr.name();
			try {
				attrToS = McToS.attrToS.get(attr);
			} catch (NullPointerException ex) {
			}
			rtn = String.format("%s%s %s,", rtn, attrToS,
					attrModifToString((Collection<AttributeModifier>) attrs.values().toArray()[i]));
			i++;
		}
		return rtn;
	}

	private static String enchsToString(Map<Enchantment, Integer> enchs) {
		String rtn = "";
		int i = 0;
		for (Enchantment ench : enchs.keySet()) {
			String enchToS = ench.toString();
			try {
				enchToS = McToS.enchToS.get(ench);
			} catch (NullPointerException ex) {
			}
			rtn = String.format("%s%s %s,", rtn, enchToS, enchs.values().toArray()[i]);
			i++;
		}
		return rtn;
	}

	private static String itemToString(ItemStack item) {
		String attrsToS = "";
		try {
			attrsToS = attrToString(item.getItemMeta().getAttributeModifiers().asMap());
		} catch (NullPointerException ex) {
		}

		String enchsToS = "";
		try {
			enchsToS = enchsToString(item.getItemMeta().getEnchants());
		} catch (NullPointerException ex) {
		}

		String displayName = "";
		try {
			displayName = item.getItemMeta().getDisplayName();
		} catch (NullPointerException ex) {
		}

		int amount = 1;
		try {
			amount = item.getAmount();
		} catch (NullPointerException ex) {
		}

		int dmg = 0;
		try {
			Damageable imdmg = (Damageable) item.getItemMeta();
			dmg = imdmg.getDamage();
		} catch (NullPointerException ex) {
		}

		return String.format("%s %s , %s; %s%s%s\n", amount, item.getType(),
				dmg == 0 ? "" : String.format("Durability=%d", item.getType().getMaxDurability() - dmg),
				displayName == "" ? "" : String.format("Name : %s; ", displayName),
				enchsToS == "" ? "" : String.format("Enchantments : %s; ", enchsToS),
				attrsToS == "" ? "" : String.format("Attributes : %s", attrsToS));
	}

	/**
	 * Describe items in a better manner than the .toString() does
	 * 
	 * @param isStack Items you want to be described correctly
	 * @return String that ends with a \n
	 */
	public static String itemsToString(ItemStack[] isStack) {
		String rtn = "";
		for (ItemStack iStack : isStack) {
			rtn = String.format("%s%s", rtn, itemToString(iStack));
		}
		return rtn;
	}

	private static String effToString(PotionEffectType eff, int amp) {
		String rtn = "";
		rtn = String.format("%s %d", eff.getName(), amp + 1);
		return rtn;
	}

	/**
	 * Describe effects in a better manner than the .toString() does
	 * 
	 * @param Effects you want to be described correctly
	 * @return String that ends with a \n
	 */
	public static String effToString(LinkedHashSet<Pair<PotionEffectType, Integer>> effs) {
		String rtn = "";
		for (Pair<PotionEffectType, Integer> pair : effs) {
			try {
			rtn = String.format("%s%s\n", rtn, effToString(pair.getLeft(), pair.getRight()));
			}catch(NullPointerException ex) {
			}
		}
		return rtn;
	}
}
