package fr.swarmyyyy.utilities.mc;

import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeModifier;
import org.bukkit.craftbukkit.libs.org.apache.commons.lang3.tuple.Pair;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;

public final class Metas {
	/**
	 * 
	 * @param mat  The Material, which will turn into one Item, which you will apply
	 *             the Metas to
	 * @param name The Name the Item will have, if set to null, do not set one
	 * @return The Item with its Metas applied
	 */
	public static ItemStack setName(Material mat, String name) {
		return setMetasFull(new ItemStack[] { new ItemStack(mat) }, name, null, null)[0];
	}

	/**
	 * 
	 * @param mat  The Material, which will turn into one Item, which you will apply
	 *             the Metas to
	 * @param name The Name the Item will have, if set to null, do not set one
	 * @return The Item with its Metas applied
	 */
	public static ItemStack setName(ItemStack item, String name) {
		return setMetasFull(new ItemStack[] { item }, name, null, null)[0];
	}

	/**
	 * 
	 * @param mat  The Material, which will turn into one Item, which you will apply
	 *             the Metas to
	 * @param name The Name the Item will have, if set to null, do not set one
	 * @return The Item with its Metas applied
	 */
	public static ItemStack[] setNames(ItemStack[] items, String name) {
		return setMetasFull(items, name, null, null);
	}

	/**
	 * 
	 * @param mat   The Material, which will turn into one Item, which you will
	 *              apply the Metas to
	 * @param name  The Name the Item will have, if set to null, do not set one
	 * @param attrs The Attributes the Item will have, if set to null, do not set
	 *              any
	 * @return The Item with its Metas applied
	 */
	public static ItemStack setAttrs(Material mat, String name, Pair<Attribute, AttributeModifier>[] attrs) {
		return setMetasFull(new ItemStack[] { new ItemStack(mat) }, name, null, attrs)[0];
	}

	/**
	 * 
	 * @param mat  The Material, which will turn into one Item, which you will apply
	 *             the Metas to
	 * @param name The Name the Item will have, if set to null, do not set one
	 * @param attr The Attribute the Item will have, if set to null, do not set any
	 * @return The Item with its Metas applied
	 */
	@SuppressWarnings("unchecked")
	public static ItemStack setAttr(Material mat, String name, Pair<Attribute, AttributeModifier> attr) {
		return setMetasFull(new ItemStack[] { new ItemStack(mat) }, name, null, new Pair[] { attr })[0];
	}

	/**
	 * 
	 * @param mat  The Material, which will turn into one Item, which you will apply
	 *             the Metas to
	 * @param attr The Attribute the Item will have, if set to null, do not set any
	 * @return The Item with its Metas applied
	 */
	@SuppressWarnings("unchecked")
	public static ItemStack setAttr(Material mat, Pair<Attribute, AttributeModifier> attr) {
		return setMetasFull(new ItemStack[] { new ItemStack(mat) }, null, null, new Pair[] { attr })[0];
	}

	/**
	 * 
	 * @param mat   The Material, which will turn into one Item, which you will
	 *              apply the Metas to
	 * @param name  The Name the Item will have, if set to null, do not set one
	 * @param enchs The Enchantments the Item will have, if set to null, do not set
	 *              any
	 * @return The Item with its Metas applied
	 */
	public static ItemStack setEnchs(Material mat, String name, Pair<Enchantment, Integer>[] enchs) {
		return setMetasFull(new ItemStack[] { new ItemStack(mat) }, name, enchs, null)[0];
	}

	/**
	 * Set numerous Metas to an Item, making it a one line command instead of
	 * enlarging your code
	 * 
	 * @param mat  The Material, which will turn into one Item, which you will apply
	 *             the Metas to
	 * @param name The Name the Item will have, if set to null, do not set one
	 * @param ench The Enchantment the Item will have, if set to null, do not set
	 *             any
	 * @return The Item with its Metas applied
	 */
	@SuppressWarnings("unchecked")
	public static ItemStack setEnch(Material mat, String name, Pair<Enchantment, Integer> ench) {
		return setMetasFull(new ItemStack[] { new ItemStack(mat) }, name, new Pair[] { ench }, null)[0];
	}

	/**
	 * Set numerous Metas to an Item, making it a one line command instead of
	 * enlarging your code
	 * 
	 * @param mat  The Material, which will turn into one Item, which you will apply
	 *             the Metas to
	 * @param ench The Enchantment the Item will have, if set to null, do not set
	 *             any
	 * @return The Item with its Metas applied
	 */
	@SuppressWarnings("unchecked")
	public static ItemStack setEnch(Material mat, Pair<Enchantment, Integer> ench) {
		return setMetasFull(new ItemStack[] { new ItemStack(mat) }, null, new Pair[] { ench }, null)[0];
	}

	/**
	 * Set numerous Metas to an Item, making it a one line command instead of
	 * enlarging your code
	 * 
	 * @param mat   The Material, which will turn into one Item, which you will
	 *              apply the Metas to
	 * @param enchs The Enchantments the Item will have, if set to null, do not set
	 *              any
	 * @return The Item with its Metas applied
	 */
	public static ItemStack setEnchs(Material mat, Pair<Enchantment, Integer>[] enchs) {
		return setMetasFull(new ItemStack[] { new ItemStack(mat) }, null, enchs, null)[0];
	}

	/**
	 * Set numerous Metas to numerous Items, making it a one line command instead of
	 * enlarging your code
	 * 
	 * @param items The Items you will apply the Metas to
	 * @param enchs The Enchantments the Item will have, if set to null, do not set
	 *              any
	 * @return The Items with its Metas applied
	 */
	public static ItemStack[] setEnchs(ItemStack items[], Pair<Enchantment, Integer>[] enchs) {
		return setMetasFull(items, null, enchs, null);
	}

	/**
	 * Set numerous Metas to numerous Items, making it a one line command instead of
	 * enlarging your code
	 * 
	 * @param items The Items you will apply the Metas to
	 * @param ench  The Enchantment the Items will have, if set to null, do not set
	 *              any
	 * @return The Items with its Metas applied
	 */
	@SuppressWarnings("unchecked")
	public static ItemStack[] setEnch(ItemStack[] items, String name, Pair<Enchantment, Integer> ench) {
		return setMetasFull(items, name, new Pair[] { ench }, null);
	}

	/**
	 * Set numerous Metas to numerous Items, making it a one line command instead of
	 * enlarging your code
	 * 
	 * @param items The Item you will apply the Metas to
	 * @param enchs The Enchantments the Item will have, if set to null, do not set
	 *              any
	 * @return The Items with its Metas applied
	 */
	public static ItemStack setEnchs(ItemStack itemStack, Pair<Enchantment, Integer>[] enchs) {
		return setMetasFull(new ItemStack[] { itemStack }, null, enchs, null)[0];
	}

	/**
	 * Set numerous Metas to numerous Items, making it a one line command instead of
	 * enlarging your code
	 * 
	 * @param items    The Items you will apply the Metas to
	 * @param name     The Name the Item will have, if set to null, do not set one
	 * @param enchsThe Enchantment the Items will have, if set to null, do not set
	 *                 any
	 * @return
	 */
	public static ItemStack[] setEnchs(ItemStack[] items, String name, Pair<Enchantment, Integer> enchs[]) {
		return setMetasFull(items, name, enchs, null);
	}

	/**
	 * Set numerous Metas to numerous Items, making it a one line command instead of
	 * enlarging your code
	 * 
	 * @param items The Items you will apply the Metas to
	 * @param attrs The Attributes the Item will have, if set to null, do not set
	 *              any
	 * @return The Items with its Metas applied
	 */
	public static ItemStack[] setAttrs(ItemStack items[], Pair<Attribute, AttributeModifier>[] attrs) {
		return setMetasFull(items, null, null, attrs);
	}

	/**
	 * Set numerous Metas to numerous Items, making it a one line command instead of
	 * enlarging your code
	 * 
	 * @param items The Items you will apply the Metas to
	 * @param ench  The Enchantments the Item will have, if set to null, do not set
	 *              any
	 * @return The Items with its Metas applied
	 */
	@SuppressWarnings("unchecked")
	public static ItemStack[] setEnch(ItemStack items[], Pair<Enchantment, Integer> ench) {
		return setMetasFull(items, null, new Pair[] { ench }, null);
	}

	/**
	 * Set numerous Metas to numerous Items, making it a one line command instead of
	 * enlarging your code
	 * 
	 * @param items The Items you will apply the Metas to
	 * @param attr  The Attributes the Item will have, if set to null, do not set
	 *              any
	 * @return The Items with its Metas applied
	 */
	@SuppressWarnings("unchecked")
	public static ItemStack[] setAttr(ItemStack items[], Pair<Attribute, AttributeModifier> attr) {
		return setMetasFull(items, null, null, new Pair[] { attr });
	}

	/**
	 * Set numerous Metas to an Item, making it a one line command instead of
	 * enlarging your code
	 * 
	 * @param mat  The Material, which will turn into one Item, which you will apply
	 *             the Metas to
	 * @param ench The Enchantment the Item will have, if set to null, do not set
	 *             any
	 * @param attr The Attribute the Item will have, if set to null, do not set any
	 * @return The Item with its Metas applied
	 */
	@SuppressWarnings("unchecked")
	public static ItemStack setMetas(Material mat, Pair<Enchantment, Integer> ench,
			Pair<Attribute, AttributeModifier> attr) {
		return setMetasFull(new ItemStack[] { new ItemStack(mat) }, null, new Pair[] { ench }, new Pair[] { attr })[0];
	}

	/**
	 * Set numerous Metas to an Item, making it a one line command instead of
	 * enlarging your code
	 * 
	 * @param mat  The Material, which will turn into one Item, which you will apply
	 *             the Metas to
	 * @param name The Name the Item will have, if set to null, do not set one
	 * @param ench The Enchantment the Item will have, if set to null, do not set
	 *             any
	 * @param attr The Attribute the Item will have, if set to null, do not set any
	 * @return The Item with its Metas applied
	 */
	@SuppressWarnings("unchecked")
	public static ItemStack setMetas(Material mat, String name, Pair<Enchantment, Integer> ench,
			Pair<Attribute, AttributeModifier> attr) {
		return setMetasFull(new ItemStack[] { new ItemStack(mat) }, name, new Pair[] { ench }, new Pair[] { attr })[0];
	}

	/**
	 * Set numerous Metas to an Item, making it a one line command instead of
	 * enlarging your code
	 * 
	 * @param item The Item you will apply the Metas to
	 * @param ench The Enchantment the Item will have, if set to null, do not set
	 *             any
	 * @param attr The Attribute the Item will have, if set to null, do not set any
	 * @return The Item with its Metas applied
	 */
	@SuppressWarnings("unchecked")
	public static ItemStack setMetas(ItemStack item, Pair<Enchantment, Integer> ench,
			Pair<Attribute, AttributeModifier> attr) {
		return setMetasFull(new ItemStack[] { item }, null, new Pair[] { ench }, new Pair[] { attr })[0];
	}

	/**
	 * Set numerous Metas to an Item, making it a one line command instead of
	 * enlarging your code
	 * 
	 * @param item The Item you will apply the Metas to
	 * @param name The Name the Item will have, if set to null, do not set one
	 * @param ench The Enchantment the Item will have, if set to null, do not set
	 *             any
	 * @param attr The Attribute the Item will have, if set to null, do not set any
	 * @return The Item with its Metas applied
	 */
	@SuppressWarnings("unchecked")

	public static ItemStack setMetas(ItemStack item, String name, Pair<Enchantment, Integer> ench,
			Pair<Attribute, AttributeModifier> attr) {
		return setMetasFull(new ItemStack[] { item }, name, new Pair[] { ench }, new Pair[] { attr })[0];
	}

	/**
	 * Set numerous Metas to an Item, making it a one line command instead of
	 * enlarging your code
	 * 
	 * @param mat   The Material, which will turn into one Item, which you will
	 *              apply the Metas to
	 * @param enchs The Enchantments the Item will have, if set to null, do not set
	 *              any
	 * @param attrs The Attributes the Item will have, if set to null, do not set
	 *              any
	 * @return The Item with its Metas applied
	 */
	public static ItemStack setMetas(Material mat, Pair<Enchantment, Integer>[] enchs,
			Pair<Attribute, AttributeModifier>[] attrs) {
		return setMetasFull(new ItemStack[] { new ItemStack(mat) }, null, enchs, attrs)[0];
	}

	/**
	 * Set numerous Metas to an Item, making it a one line command instead of
	 * enlarging your code
	 * 
	 * @param item  The Item you will apply the Metas to
	 * @param enchs The Enchantments the Item will have, if set to null, do not set
	 *              any
	 * @param attrs The Attributes the Item will have, if set to null, do not set
	 *              any
	 * @return The Item with its Metas applied
	 */
	public static ItemStack setMetas(ItemStack item, Pair<Enchantment, Integer>[] enchs,
			Pair<Attribute, AttributeModifier>[] attrs) {
		return setMetasFull(new ItemStack[] { item }, null, enchs, attrs)[0];
	}

	/**
	 * Set numerous Metas to an Item, making it a one line command instead of
	 * enlarging your code
	 * 
	 * @param item  The Item you will apply the Metas to
	 * @param name  The Name the Item will have, if set to null, do not set one
	 * @param enchs The Enchantments the Item will have, if set to null, do not set
	 *              any
	 * @param attrs The Attributes the Item will have, if set to null, do not set
	 *              any
	 * @return The Item with its Metas applied
	 */
	public static ItemStack setMetas(ItemStack item, String name, Pair<Enchantment, Integer>[] enchs,
			Pair<Attribute, AttributeModifier>[] attrs) {
		return setMetasFull(new ItemStack[] { item }, name, enchs, attrs)[0];
	}

	/**
	 * Set numerous Metas to numerous Items, making it a one line command instead of
	 * enlarging your code
	 * 
	 * @param items The Items you will apply the Metas to
	 * @param enchs The Enchantments the Item will have, if set to null, do not set
	 *              any
	 * @param attrs The Attributes the Item will have, if set to null, do not set
	 *              any
	 * @return The Items with its Metas applied
	 */
	public static ItemStack[] setMetas(ItemStack items[], Pair<Enchantment, Integer>[] enchs,
			Pair<Attribute, AttributeModifier>[] attrs) {
		return setMetasFull(items, null, enchs, attrs);
	}

	/**
	 * Set numerous Metas to numerous Items, making it a one line command instead of
	 * enlarging your code
	 * 
	 * @param items The Items you will apply the Metas to
	 * @param enchs The Enchantments the Item will have, if set to null, do not set
	 *              any
	 * @param attrs The Attributes the Item will have, if set to null, do not set
	 *              any
	 * @return The Items with its Metas applied
	 */
	@SuppressWarnings("unchecked")
	public static ItemStack[] setMetas(ItemStack items[], String name, Pair<Enchantment, Integer> enchs,
			Pair<Attribute, AttributeModifier> attrs) {
		return setMetasFull(items, name, new Pair[] { enchs }, new Pair[] { attrs });
	}

	/**
	 * Set numerous Metas to numerous Items, making it a one line command instead of
	 * enlarging your code
	 * 
	 * @param items The Items you will apply the Metas to
	 * @param ench  The Enchantments the Item will have, if set to null, do not set
	 *              any
	 * @param attr  The Attributes the Item will have, if set to null, do not set
	 *              any
	 * @return The Items with its Metas applied
	 */
	@SuppressWarnings("unchecked")
	public static ItemStack[] setMetas(ItemStack items[], Pair<Enchantment, Integer> ench,
			Pair<Attribute, AttributeModifier> attr) {
		return setMetasFull(items, null, new Pair[] { ench }, new Pair[] { attr });
	}

	/**
	 * Set numerous Metas to numerous Items, making it a one line command instead of
	 * enlarging your code
	 * 
	 * @param item  The Items you will apply the Metas to
	 * @param name  The Name the Item will have, if set to null, do not set one
	 * @param enchs The Enchantments the Item will have, if set to null, do not set
	 *              any
	 * @param attrs The Attributes the Item will have, if set to null, do not set
	 *              any
	 * @return The Items with its Metas applied
	 */
	public static ItemStack[] setMetasFull(ItemStack items[], String name, Pair<Enchantment, Integer>[] enchs,
			Pair<Attribute, AttributeModifier>[] attrs) {
		for (ItemStack item : items) {
			ItemMeta im = item.getItemMeta();
			if (name != null) {
				im.setDisplayName(name);
			}

			if (enchs != null) {
				for (Pair<Enchantment, Integer> ench : enchs) {
					im.addEnchant(ench.getLeft(), ench.getRight(), true);
				}
			}

			if (attrs != null) {
				for (Pair<Attribute, AttributeModifier> attribute : attrs) {
					im.addAttributeModifier(attribute.getLeft(), attribute.getRight());
				}
			}
			item.setItemMeta(im);
		}
		return items;
	}

	public static ItemStack[] dealDamage(ItemStack items[], int damage) {
		for (ItemStack item : items) {
			Damageable imdmg = (Damageable) item.getItemMeta();
			imdmg.setDamage(damage);
			item.setItemMeta((ItemMeta) imdmg);
		}
		return items;
	}

	public static ItemStack dealDamage(ItemStack item, int damage) {
		return dealDamage(new ItemStack[] { item }, damage)[0];
	}

	public static ItemStack[] setDamage(ItemStack items[], int damage) {
		for (ItemStack item : items) {
			item = dealDamage(new ItemStack[] { item }, item.getType().getMaxDurability() - damage)[0];
		}
		return items;
	}

	public static ItemStack setDamage(ItemStack item, int damage) {
		return dealDamage(new ItemStack[] { item }, item.getType().getMaxDurability() - damage)[0];
	}

}
