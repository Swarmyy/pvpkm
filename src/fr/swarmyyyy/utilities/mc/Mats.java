package fr.swarmyyyy.utilities.mc;

import java.util.Hashtable;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public final class Mats {

	/**
	 * All the Suits relied by their material used to their craft
	 */
	public static Hashtable<Material, Material[]> suits = new Hashtable<Material, Material[]>() {
		private static final long serialVersionUID = -7667233066506724144L;
		{
			put(Material.LEATHER, new Material[] { Material.LEATHER_HELMET, Material.LEATHER_BOOTS,
					Material.LEATHER_LEGGINGS, Material.LEATHER_CHESTPLATE });

			put(Material.IRON_INGOT, new Material[] { Material.IRON_HELMET, Material.IRON_BOOTS,
					Material.IRON_CHESTPLATE, Material.IRON_LEGGINGS });

			put(Material.GOLD_INGOT, new Material[] { Material.GOLDEN_HELMET, Material.GOLDEN_BOOTS,
					Material.GOLDEN_CHESTPLATE, Material.GOLDEN_LEGGINGS });

			put(Material.FIRE, new Material[] { Material.CHAINMAIL_HELMET, Material.CHAINMAIL_BOOTS,
					Material.CHAINMAIL_CHESTPLATE, Material.CHAINMAIL_LEGGINGS });

			put(Material.DIAMOND, new Material[] { Material.DIAMOND_HELMET, Material.DIAMOND_BOOTS,
					Material.DIAMOND_CHESTPLATE, Material.DIAMOND_LEGGINGS });
		}
	};

	/**
	 * Get the Suit in relation with the Suit Component
	 * 
	 * @param suitComp The Material used to craft the Suit
	 * @return The Suit
	 */
	public static ItemStack[] getFullSuit(Material suitComp) {
		ItemStack[] suit;
		try {
			suit = new ItemStack[suits.get(suitComp).length];
		} catch (NullPointerException ex) {
			return new ItemStack[0];
		}
		for (int i = 0; i < suits.get(suitComp).length; i++) {
			suit[i] = new ItemStack(suits.get(suitComp)[i]);
		}
		return suit;
	}

}
