package fr.swarmyyyy.utilities.mc;

import java.util.Hashtable;

import org.bukkit.attribute.Attribute;

public final class Base {
	
	private static Hashtable<Attribute, Double> AttrBase = new Hashtable<Attribute, Double>() {
		private static final long serialVersionUID = -2930567016245074581L;
		{
			put(Attribute.GENERIC_MAX_HEALTH, 20.0);
		}
	};

	public static Hashtable<Attribute, Double> getAttrBase() {
		return AttrBase;
	}
	
}
