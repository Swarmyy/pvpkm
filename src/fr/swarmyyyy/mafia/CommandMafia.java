package fr.swarmyyyy.mafia;

import java.util.Hashtable;
import java.util.Random;

import org.apache.commons.lang.ArrayUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import fr.swarmyyyy.SwPlayer;
import fr.swarmyyyy.swplugin;
import fr.swarmyyyy.mafia.core.roles.IAbility;
import fr.swarmyyyy.mafia.core.roles.IAttribute;
import fr.swarmyyyy.mafia.core.roles.Role;

public class CommandMafia implements CommandExecutor {

	// private mafia game;

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String msg, String[] args) {
		if (cmd.getName().equalsIgnoreCase("mafia")) {
			if (args.length < 1) {
				sender.sendMessage(noArgs(null));
			} else {
				Player p = null;
				if (sender instanceof Player) {
					p = (Player) sender;
				}

				try {
					switch (args[0].replaceAll("[^a-zA-Z0-9_-]", "").toLowerCase()) {
					case "role":
						p.sendMessage(role(p));
						break;
					case "start":
						Bukkit.broadcastMessage(start(sender));
						break;
					default:
						try {
							p.sendMessage(arg1(args[0], p, args[1].replaceAll("[^a-zA-Z0-9_-]", "").toLowerCase(),
									(String[]) ArrayUtils.subarray(args, 2, ArrayUtils.getLength(args))));
						} catch (ArrayIndexOutOfBoundsException ex) {
							p.sendMessage("You need to specify a target");
						}
					}

				} catch (NullPointerException ex) {
					sender.sendMessage("You're not a player");
				}
			}
			return true;
		}
		return false;
	}

	private static String start(CommandSender sender) {
		// Used to instantiate roleComp
		@SuppressWarnings("unused")
		mafia game = mafia.getInstance();

		Hashtable<Role, Integer> roleComp = mafia.getRoleComp();
		for (SwPlayer swp : SwPlayer.getListPlayers()) {
			if (Bukkit.getServer().getPlayer(swp.getUuid()) != null) {
				Role role = (Role) roleComp.keySet().toArray()[new Random().nextInt(roleComp.size())];
				swp.setRole(role);
				roleComp.put(role, roleComp.get(role) - 1);
			}
		}
		Bukkit.getServer().getWorld("world").setTime(0L);
		mafia.getInstance().setGameStarted(true);
		return "Mafia started";
	}

	private static String role(Player p) {
		Role role = SwPlayer.getSPlayer(p).getRole();
		String rtn = String.format("�4%s�r\n%s\nGoal : %s\n", role.getName(), role.getSummary(),
				SwPlayer.getSPlayer(p).getRole().getGoal());
		for (IAbility abil : role.getAbilities()) {
			rtn = String.format("%s%s : %s, Night : %s, Day : %s, %s, \n", rtn, abil.name(), abil.desc(),
					abil.isNight(), abil.isDay(),
					abil.priority() == Byte.MAX_VALUE ? "Actions occurs at the end of the night"
							: String.format("Priority : %d", abil.priority()));
		}
		return rtn;
	}

	private static String arg1(String arg0, Player p, String arg1, String... args) {
		arg0 = arg0.replaceAll("[^a-zA-Z0-9_-]", "").toLowerCase();
		arg1 = arg1.replaceAll("[^a-zA-Z0-9_-]", "").toLowerCase();
		if (arg0.equals("vote")) {
			return vote(arg0, p, arg1, args);
		}
		Bukkit.getLogger().info(arg0);
		return exec(arg0, p, arg1, args);
	}

	@SuppressWarnings("unchecked")
	private static String exec(String abil, Player caster, String target, String... args) {
		IAbility ability = null;
		Bukkit.getLogger().info(abil);
		for (IAbility tAbil : SwPlayer.getSPlayer(caster).getRole().getAbilities()) {
			Bukkit.getLogger().info(tAbil.name());
			if (tAbil.name().toLowerCase().equals(abil)) {
				ability = tAbil;
			}
		}
		if (ability == null) {
			return "You need to specify a correct ability name";
		}

		Player ptarget = null;
		for (Player p : Bukkit.getServer().getOnlinePlayers()) {
			ptarget = p.getName().toLowerCase().equals(target) ? p : null;
		}
		if (ptarget == null) {
			return "You need to specify a target";
		}

		long time = Bukkit.getServer().getWorld("world").getTime();
		if ((ability.isDay() && time < 13000) || (ability.isNight() && time >= 13000)) {
			String rtnRepercussion = "";
			for (@SuppressWarnings("rawtypes")
			IAttribute attribute : SwPlayer.getSPlayer(ptarget).getAttributes()) {
				rtnRepercussion = attribute.repercussion(caster, ptarget, ability, args);
			}

			if (rtnRepercussion.equals("")) {
				return ability.exec(caster, ptarget, args);
			}
			return rtnRepercussion;
		}
		return String.format("Ability needs to be executed at %s", ability.isDay() ? "daytime" : "nighttime");
	}

	private static String vote(String arg1, Player caster, String arg2, String... args) {
		if (mafia.getInstance().isVoting()) {
			SwPlayer target = null;
			try {
				target = SwPlayer.getSPlayer(Bukkit.getPlayer(arg2));
			} catch (NullPointerException ex) {
				return "Enter a valid player name";
			}

			int votes = 0;
			try {
				votes = mafia.getInstance().votes.get(target);
			} catch (NullPointerException ex) {
			}

			mafia.getInstance().votes.put(target, votes + 1);
			return "Your vote was sucessfully comptabilised";
		}
		return "Voting phase is only available the 5 first minutes of the day";
	}

	private static String noArgs(Player p) {
		if (p != null) {

		}
		return "Usage :";
	}

}
