package fr.swarmyyyy.mafia.core.roles;

import org.bukkit.entity.Player;

public interface IAttribute<TRPargs> {

	public String name();

	public String desc();

	public String repercussion(Player targeting, Player target, IAbility targetAbility, TRPargs args);
}
