package fr.swarmyyyy.mafia.core.roles;

import java.util.HashSet;
import java.util.Hashtable;

import org.bukkit.entity.Player;

import fr.swarmyyyy.SwPlayer;
import fr.swarmyyyy.mafia.mafia;
import fr.swarmyyyy.mafia.core.Alignment;
import fr.swarmyyyy.mafia.core.Categorie;

public class Role {

	private static HashSet<Role> roles = new HashSet<Role>();

	public static HashSet<Role> getRoles() {
		return roles;
	}

	public static final Role BODYGUARD = new Role("Bodyguard", Alignment.TOWN,
			new Categorie[] { Categorie.Protective, Categorie.Killing }, new IAbility() {

				@Override
				public String name() {
					return "Guard";
				}

				@Override
				public String desc() {
					return "Guard one player each night. If someone attacks a guarded player, both the attacker and the Bodyguard loose 5 hearts instead of the guarded player.";
				}

				@Override
				public byte priority() {
					return Byte.MIN_VALUE;
				}

				@Override
				public String exec(Player caster, Player target, String... args) {
					if (caster.getUniqueId() == target.getUniqueId()) {
						return "You can't guard yourself!";
					}
					addAttr(new IAttribute<Void>() {

						@Override
						public String name() {
							return "Guarded";
						}

						@Override
						public String desc() {
							return "If someone attacks a guarded player, both the attacker and the Bodyguard die instead of the guarded player.";
						}

						@Override
						public String repercussion(Player targeting, Player target, IAbility targetingAbility,
								Void args) {
							if (targetingAbility.isLethal()) {
								caster.setHealth(caster.getHealth() - 10.0);
								targeting.setHealth(targeting.getHealth() - 10.0);
								return "This player was being guarded, your health is decreased by 5 hearts, but its guardian's too";
							}
							return "";
						}

					}, target);
					return String.format("%s is now guarded, if it gets any damage ", target.getName());
				}

			}, "Lynch every criminal and evildoer", "A war veteran who secretly makes a living by selling protection.",
			new Hashtable<Investigator, String>() {
				private static final long serialVersionUID = 1L;
				{
					put(Investigator.SHERIFF, "Not Suspicious");
					put(Investigator.INVESTIGATOR, "Not Suspicious");
				}
			});

	public static final Role SHERIFF = new Role("Sheriff", Alignment.TOWN, Categorie.Investigative, new IAbility() {
		@Override
		public String name() {
			return "Look";
		}

		@Override
		public String desc() {
			return "Check one player each day for its affiliation of last night.";
		}

		@Override
		public byte priority() {
			return Byte.MAX_VALUE;
		}

		@Override
		public boolean isDay() {
			return true;
		}

		@Override
		public boolean isNight() {
			return false;
		}

		@Override
		public String exec(Player caster, Player target, String... args) {
			return String.format("%s is %s", target.getName(),
					SwPlayer.getSPlayer(target).getRole().investigationResults.get(Investigator.SHERIFF));
		}

	}, "Lynch every criminal and evildoer",
			"A member of law enforcement, forced into hiding because of the threat of murder.",
			new Hashtable<Investigator, String>() {
				private static final long serialVersionUID = 1L;
				{
					put(Investigator.SHERIFF, "Not Suspicious");
					put(Investigator.INVESTIGATOR, "No Crime");
				}
			});

	public static final Role INVESTIGATOR = new Role("Investigator", Alignment.TOWN, Categorie.Investigative,
			new IAbility() {

				@Override
				public String name() {
					return "Investigate";
				}

				@Override
				public String desc() {
					return "Check one player each day for that player's criminal record during the night.";
				}

				@Override
				public byte priority() {
					return Byte.MAX_VALUE;
				}

				@Override
				public boolean isDay() {
					return true;
				}

				@Override
				public boolean isNight() {
					return false;
				}

				@Override
				public String exec(Player caster, Player target, String... args) {
					String message;
					if (SwPlayer.getSPlayer(target).getCriminalActivity().contains(mafia.getInstance().getNights())) {
						message = String.format("%s has committed %s tonight", target.getName(),
								SwPlayer.getSPlayer(target).getRole().investigationResults
										.get(Investigator.INVESTIGATOR));
					} else {
						message = String.format("%s has not commited anything tonight", target.getName());
					}
					return message;
				}

			}, "Lynch every criminal and evildoer", "A private sleuth, discreetly aiding the townsfolk.",
			new Hashtable<Investigator, String>() {
				private static final long serialVersionUID = 1L;
				{
					put(Investigator.SHERIFF, "Not Suspicious");
					put(Investigator.INVESTIGATOR, "Trespassing");
				}
			});

	private static boolean addAttr(@SuppressWarnings("rawtypes") IAttribute attr, Player p) {
		if (SwPlayer.getSPlayer(p).getAttributes().contains(attr)) {
			return false;
		}
		SwPlayer.getSPlayer(p).getAttributes().add(attr);
		return SwPlayer.getSPlayer(p).getAttributes().contains(attr);
	}

	private static Hashtable<Investigator, Role> investToRole = new Hashtable<Investigator, Role>() {
		private static final long serialVersionUID = -6899368988165944283L;
		{
			put(Investigator.SHERIFF, SHERIFF);
			put(Investigator.INVESTIGATOR, INVESTIGATOR);
		}
	};

	protected String name;

	public String getName() {
		return name;
	}

	protected String goal;

	public String getGoal() {
		return goal;
	}

	protected String summary;

	public String getSummary() {
		return summary;
	}

	protected HashSet<IAbility> abilities;

	public HashSet<IAbility> getAbilities() {
		return abilities;
	}

	protected Alignment alignment;

	public Alignment getAlignment() {
		return alignment;
	}

	protected HashSet<Categorie> categories;

	public HashSet<Categorie> getCategories() {
		return categories;
	}

	@SuppressWarnings("rawtypes")
	protected HashSet<IAttribute> attributes;

	@SuppressWarnings("rawtypes")
	public HashSet<IAttribute> getAttributes() {
		return attributes;
	}

	private Hashtable<Investigator, String> investigationResults;

	protected Role(String name, Alignment alignment, Categorie categorie, IAbility ability, String goal, String summary,
			Hashtable<Investigator, String> investigationResults) {
		setRole(name, alignment, new Categorie[] { categorie }, new IAbility[] { ability }, new IAttribute[0], goal,
				summary, investigationResults);
	}

	protected Role(String name, Alignment alignment, Categorie[] categories, IAbility ability, String goal,
			String summary, Hashtable<Investigator, String> investigationResults) {
		setRole(name, alignment, categories, new IAbility[] { ability }, new IAttribute[0], goal, summary,
				investigationResults);
	}

	@SuppressWarnings("rawtypes")
	protected Role(String name, Alignment alignment, Categorie[] categories, IAbility[] abilities,
			IAttribute[] attributes, String goal, String summary,
			Hashtable<Investigator, String> investigationResults) {
		setRole(name, alignment, categories, abilities, attributes, goal, summary, investigationResults);
	}

	@SuppressWarnings("rawtypes")
	private void setRole(String name, Alignment alignment, Categorie[] categories, IAbility[] abilities,
			IAttribute[] attributes, String goal, String summary,
			Hashtable<Investigator, String> investigationResults) {
		this.name = name;

		this.alignment = alignment;

		this.categories = new HashSet<Categorie>();
		for (Categorie cat : categories) {
			this.categories.add(cat);
		}

		this.abilities = new HashSet<IAbility>();
		for (IAbility ability : abilities) {
			this.abilities.add(ability);
		}

		this.attributes = new HashSet<IAttribute>();
		for (IAttribute attribute : attributes) {
			this.attributes.add(attribute);
		}

		this.investigationResults = new Hashtable<Investigator, String>(investigationResults);

		this.goal = goal;
		this.summary = summary;

		roles.add(this);

	}

}
