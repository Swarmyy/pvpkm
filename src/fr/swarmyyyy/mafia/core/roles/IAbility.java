package fr.swarmyyyy.mafia.core.roles;

import org.bukkit.entity.Player;

/**
 * 
 * @author Swarmyyyy
 * @note All args must be represented ideally by a tuple for better
 *       comprehension or an object
 */
public interface IAbility {

	/**
	 * The lower the first it's executed
	 * 
	 * @return the priority, if byte maximum value, then it is done at the end the
	 *         night
	 */
	public byte priority();

	public String name();

	public String desc();

	/**
	 * I advise you to not @Override it
	 * 
	 * @param uses
	 * @return The number of uses this ability have still left. -1 means each night
	 *         it can be used
	 */
	public default byte usesLeft(byte uses) {
		return (byte) (-uses() - uses);
	}

	public default byte uses() {
		return -1;
	}

	public default boolean isNight() {
		return true;
	}

	public default boolean isDay() {
		return false;
	}

	public default boolean isLethal() {
		return false;
	}

	public String exec(Player caster, Player target, String... args);

}
