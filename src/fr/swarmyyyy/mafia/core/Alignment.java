package fr.swarmyyyy.mafia.core;

public enum Alignment {
	TOWN, MAFIA, TRIAD, NEUTRAL
}
