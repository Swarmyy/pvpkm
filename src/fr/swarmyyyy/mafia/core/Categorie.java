package fr.swarmyyyy.mafia.core;

public enum Categorie {
	Government, Investigative, Killing, Power, Protective, Random, Deception, Support, Benign
}
