package fr.swarmyyyy.mafia;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Player;

import fr.swarmyyyy.SwPlayer;
import fr.swarmyyyy.mafia.core.roles.Role;

public final class mafia {

	private static Hashtable<Role, Integer> roleComp = new Hashtable<Role, Integer>() {
		private static final long serialVersionUID = -2389826764744056031L;
		{
			put(Role.BODYGUARD, 1);
		}
	};

	public static Hashtable<Role, Integer> getRoleComp() {
		return roleComp;
	}

	public static void setRoleComp(Hashtable<Role, Integer> roleComp) {
		mafia.roleComp = roleComp;
	}

	private int nights = 0;

	public int getNights() {
		return nights;
	}

	private boolean voting = false;

	public boolean isVoting() {
		return voting;
	}

	public void setVoting(boolean voting) {
		this.voting = voting;
	}

	private boolean gameStarted;

	public boolean isGameStarted() {
		return gameStarted;
	}

	public void setGameStarted(boolean gameStarted) {
		this.gameStarted = gameStarted;
	}

	public Hashtable<SwPlayer, Integer> votes = new Hashtable<SwPlayer, Integer>();

	private static mafia mafiaInstance = null;

	private mafia() {

	}

	public static mafia getInstance() {
		if (mafiaInstance == null) {
			mafiaInstance = new mafia();
		}
		return mafiaInstance;

	}

	public void resolveVotes() {
		int i = 0;
		ArrayList<Integer> playersVoted = new ArrayList<Integer>();
		int maxVotes = -1;
		for (int nVotes : votes.values()) {
			if (nVotes == maxVotes) {
				playersVoted.add(i);
			} else if (nVotes > maxVotes) {
				playersVoted.clear();
				playersVoted.add(i);
				maxVotes = nVotes;
			}
			i++;
		}
		if (playersVoted.size() == 0) {
			Bukkit.broadcastMessage("Noone has voted, no hearts will be removed");
		} else {
			SwPlayer voted = (SwPlayer) votes.keySet().toArray()[playersVoted
					.get(new Random().nextInt(playersVoted.size()))];
			Player pvoted = SwPlayer.getPlayer(voted, Bukkit.getServer().getOnlinePlayers());
			pvoted.getAttribute(Attribute.GENERIC_MAX_HEALTH)
					.setBaseValue(pvoted.getAttribute(Attribute.GENERIC_MAX_HEALTH).getBaseValue() - 10.0);
			voted.setVoted(true);
			Bukkit.broadcastMessage(
					String.format("%s is the player most voted, they loose 5 hearts", pvoted.getName()));
		}
		voting = false;
	}

	public void takeTurn() {
		nights++;
	}

}
