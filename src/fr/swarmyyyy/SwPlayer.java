package fr.swarmyyyy;

import java.util.Collection;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import fr.swarmyyyy.mafia.core.roles.IAbility;
import fr.swarmyyyy.mafia.core.roles.IAttribute;
import fr.swarmyyyy.mafia.core.roles.Role;

public class SwPlayer {

	///
	/// Players
	///
	private static HashSet<SwPlayer> ListPlayers = new HashSet<SwPlayer>();

	public static HashSet<SwPlayer> getListPlayers() {
		for (Player p : Bukkit.getServer().getOnlinePlayers()) {
			new SwPlayer(p);
		}
		return ListPlayers;
	}

	///
	/// mafia
	///
	private Role role;

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	@SuppressWarnings("rawtypes")
	private HashSet<IAttribute> attributes = new HashSet<IAttribute>();

	@SuppressWarnings("rawtypes")
	public HashSet<IAttribute> getAttributes() {
		return attributes;
	}

	private boolean voted;

	public boolean isVoted() {
		return voted;
	}

	public void setVoted(boolean voted) {
		this.voted = voted;
	}

	/**
	 * Night : Used ability
	 */
	private Hashtable<Integer, IAbility> criminalActivity = new Hashtable<Integer, IAbility>();

	public Hashtable<Integer, IAbility> getCriminalActivity() {
		return criminalActivity;
	}

	///
	/// pvpkm
	///
	/**
	 * pvpkm levels
	 */
	private int level = 1;

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	private boolean inGame = false;

	public boolean isInGame() {
		return inGame;
	}

	public void setInGame(boolean inGame) {
		this.inGame = inGame;
	}

	///
	/// Generic
	///
	private static boolean LastResult = true;

	public static boolean isLastResult() {
		return LastResult;
	}

	private int permsLevel = 0;

	public int getPermsLevel() {
		return permsLevel;
	}

	private UUID uuid;

	public UUID getUuid() {
		return uuid;
	}

	public SwPlayer(Player p) {
		uuid = p.getUniqueId();
		if (p.isOp()) {
			permsLevel = 3;
		}
		LastResult = ListPlayers.add(this);
	}

	public static SwPlayer getSPlayer(Player p) {
		for (SwPlayer syp : ListPlayers) {
			if (syp.getUuid() == p.getUniqueId()) {
				return syp;
			}
		}
		return new SwPlayer(p);
	}

	/**
	 * Return the Player in the list associed to the SwPlayer
	 * 
	 * @param swp     the SwPlayer you're looking for
	 * @param players the players list you're looking in
	 * @return the player if it found it, null otherwise
	 */
	public static Player getPlayer(SwPlayer swp, Collection<? extends Player> players) {
		for (Player p : players) {
			if (p.getUniqueId() == swp.getUuid()) {
				return p;
			}
		}
		return null;
	}

}
