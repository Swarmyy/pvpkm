package fr.swarmyyyy;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import fr.swarmyyyy.mafia.CommandMafia;
import fr.swarmyyyy.mafia.mafia;
import fr.swarmyyyy.pvpkm.CommandKit;
import fr.swarmyyyy.pvpkm.pvpkm;

/*world
 * world_nether
 * world_the_end
*/

public class swplugin extends JavaPlugin {

	@Override
	public void onEnable() {
		super.onEnable();

		getCommand("kit").setExecutor(new CommandKit());
		System.out.println("Kit command instantiated");

		getCommand("mafia").setExecutor(new CommandMafia());
		System.out.println("Mafia command instantiated");

		getServer().getPluginManager().registerEvents(new MyPluginListeners(), this);
		System.out.println("Listeners instantiated");

		pvpkm.instantiateKits();
		System.out.println("Kits instantiated");

		BukkitTask mafiatask = new BukkitRunnable() {

			@Override
			public void run() {
				if (mafia.getInstance().isGameStarted()) {
					long time = Bukkit.getServer().getWorld("world").getTime();
					// day actions
					if (time < 13000) {
						if (time > 0 && time < 3250) {
							if (!mafia.getInstance().isVoting()) {
								mafia.getInstance().setVoting(true);

								Bukkit.broadcastMessage(
										"Votes are now open for 5 minutes, type /mafia vote <name> to vote the player you want");
							}
						}
						if (time > 3250) {
							if (mafia.getInstance().isVoting()) {
								mafia.getInstance().resolveVotes();
							}

						}
					}
					// night actions
					if (time >= 13000) {

					}
				}
			}
		}.runTaskTimer(this, 0, 20);

		System.out.println("Swarm's Plugin On");
	}

	@Override
	public void onDisable() {
		super.onDisable();

		System.out.println("Swarm's Plugin PVPKM Off");
	}

}
