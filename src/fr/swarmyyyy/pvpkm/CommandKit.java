package fr.swarmyyyy.pvpkm;

import org.bukkit.Bukkit;
import org.bukkit.attribute.Attribute;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.libs.org.apache.commons.lang3.tuple.Pair;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import fr.swarmyyyy.SwPlayer;
import fr.swarmyyyy.pvpkm.core.Kit;

public class CommandKit implements CommandExecutor {

	private static String errorString = "Usage : /kit get | info   <kitname> | <level>";

	private static String arrowCalc = "Arrow counts for kits having a Bow is the following : %d*level\n";

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String msg, String[] args) {
		if (sender instanceof Player) {
			Player p = (Player) sender;
			if (cmd.getName().equalsIgnoreCase("kit")) {
				if (args.length < 1) {
					p.sendMessage(errorString);
				} else {
					switch (args[0].replaceAll("[^a-zA-Z0-9_-]", "")) {
					case "get":
						try {
							p.sendMessage(get(args[1], p));
						} catch (ArrayIndexOutOfBoundsException ex) {
							p.sendMessage(String.format("Type the Kit you want to be given\nYour level : %d",
									SwPlayer.getSPlayer(p).getLevel()));
						}
						break;
					case "give":
						try {
							p.sendMessage(get(args[1], p));
						} catch (ArrayIndexOutOfBoundsException ex) {
							p.sendMessage(String.format("Type the Kit you want to be given\nYour level : %d",
									SwPlayer.getSPlayer(p).getLevel()));
						}
						break;
					case "info":
						try {
							p.sendMessage(info(args[1]));
						} catch (ArrayIndexOutOfBoundsException ex) {
							p.sendMessage("Type the level or the Kit you want to be displayed informations of");
						}
						break;
					case "admin":
						if (SwPlayer.getSPlayer(p).getPermsLevel() >= 1) {
							SwPlayer.getSPlayer(p).setInGame(false);
							SwPlayer.getSPlayer(p).setLevel(Integer.MAX_VALUE);
							p.sendMessage(String.format("Hi %s!", p.getName()));
							Bukkit.getLogger().warning(p.getName() + " used kit admin");
							try {
								SwPlayer.getSPlayer(p).setLevel(Integer.parseInt(args[1]));
							} catch (ArrayIndexOutOfBoundsException AIOOBex) {
							} catch (NullPointerException NPex) {
							}
							// p.sendMessage(Double.toString(p.getAttribute(Attribute.GENERIC_MOVEMENT_SPEED).getValue()));
						} else {
							p.sendMessage("You're not admin");
						}
						break;
					default:
						p.sendMessage(errorString);
					}
				}
			}
			return true;

		} else {
			sender.sendMessage("You aren't a player");
		}
		return false;
	}

	private static String get(String arg1, Player p) {
		arg1 = arg1.toLowerCase();
		// Bukkit.getLogger().info("Typed get " + arg1);
		// Bukkit.getLogger().info("" + Kit.getKits().toString());
		try {
			if (!SwPlayer.getSPlayer(p).isInGame()) {
				if (SwPlayer.getSPlayer(p).getLevel() >= Kit.get(arg1).getLevel()) {

					for (ItemStack i : Kit.get(arg1).getItems()) {
						p.getInventory().addItem(i);
					}

					for (Pair<PotionEffectType, Integer> eff : Kit.get(arg1).getEffects()) {
						p.addPotionEffect(new PotionEffect(eff.getLeft(), Integer.MAX_VALUE, eff.getRight()));
						if (eff.getLeft() == PotionEffectType.HEALTH_BOOST) {
							p.addPotionEffect(new PotionEffect(PotionEffectType.HEAL, 1, eff.getRight()));
						}
					}

					for (Pair<Attribute, Double> attrs : Kit.get(arg1).getPlayerAttrs()) {
						p.getAttribute(attrs.getLeft()).setBaseValue(attrs.getRight());
						if (attrs.getLeft() == Attribute.GENERIC_MAX_HEALTH) {
							p.addPotionEffect(
									new PotionEffect(PotionEffectType.HEAL, 1, (int) ((attrs.getRight() - 20) / 2)));
						}
					}

					SwPlayer.getSPlayer(p).setInGame(true);
					return "You've been given your kit";

				} else {
					return String.format(
							"You don't have a sufficient level to have this kit, yours is at %s and you need level %d",
							SwPlayer.getSPlayer(p).getLevel(), Kit.get(arg1).getLevel());
				}

			} else {
				return "You're already in-game, you need to die to get a new kit";
			}

		} catch (NullPointerException ex) {
			return String.format("Type the Kit you want to be given\nYour level : %d",
					SwPlayer.getSPlayer(p).getLevel());
		}
	}

	private static String info(String arg1) {
		try {
			// Case level
			//
			return infoLevel(Byte.parseByte(arg1));
		} catch (NumberFormatException NFex) {
			// Case kitname
			//
			// arg1.replaceAll("[^a-zA-Z0-9_-]", ""));
			arg1 = arg1.toLowerCase();
			try {
				return String.format("    �b�l%s�r\n    Level %d\n%s\n", arg1.toUpperCase(), Kit.get(arg1).getLevel(),
						Kit.kitCompToString(Kit.get(arg1)));
			} catch (NullPointerException NPex) {
			}
		}
		return "This kit don't exist";
	}

	private static String infoLevel(int level) {
		String rtn = String.format("�4�lKits at level %d�r\n", level);
		for (Kit kit : Kit.getKits()) {
			if (kit.getLevel() == level) {
				rtn = String.format("%s\n    �b�l%s�r\n%s\n", rtn, kit.getName().toUpperCase(),
						Kit.kitCompToString(kit));
			}
		}
		return rtn;
	}

}
