package fr.swarmyyyy.pvpkm;

import org.apache.commons.lang.ArrayUtils;
import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeModifier;
import org.bukkit.attribute.AttributeModifier.Operation;
import org.bukkit.craftbukkit.libs.org.apache.commons.lang3.tuple.Pair;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

import fr.swarmyyyy.pvpkm.core.Kit;
import fr.swarmyyyy.utilities.mc.Mats;
import fr.swarmyyyy.utilities.mc.Metas;

public final class pvpkm {
	/**
	 * Instantiate all the kits available to get
	 */
	@SuppressWarnings("unchecked")
	public static void instantiateKits() {
		// Kits are sorted by their original tier which might not reflect those actual
		//
		// Level 1
		//
		new Kit("Skeleton", Material.BOW);

		new Kit("Citoyen", Metas.setName(Material.WOODEN_SWORD, "�6Cut�r"));

		new Kit("ARY", Material.BOW);

		new Kit("CaveSpider", Material.WOODEN_SWORD, Pair.of(PotionEffectType.SPEED, 0),
				Pair.of(Attribute.GENERIC_MAX_HEALTH, 18.0));
		//
		// Level 2
		//
		new Kit("ATPDR2", new ItemStack[] { Metas.setName(Material.LEATHER_HELMET, "�6Armor�r"),
				Metas.setName(Material.WOODEN_SWORD, "Dagger") });

		new Kit("Spider", Metas.setName(Material.WOODEN_SWORD, "�0Tooth�r"), Pair.of(PotionEffectType.SPEED, 0));

		new Kit("Ghast", Material.BOW, 22.0);

		new Kit("Electrique", new ItemStack[] { Metas.setName(Material.WOODEN_SWORD, "Stick") }, new Pair[0],
				new Pair[] { Pair.of(Attribute.GENERIC_MAX_HEALTH, 22.0) });

		new Kit("Ghast2", Metas.setEnch(Material.BOW, Pair.of(Enchantment.ARROW_FIRE, 1)));
		//
		// Level3
		//
		new Kit("Pisto", new ItemStack[] { new ItemStack(Material.BOW), new ItemStack(Material.TNT, 64) });

		new Kit("CaveSpider2", Metas.setName(Material.WOODEN_SWORD, "�0Tooth�r"),
				new Pair[] { Pair.of(PotionEffectType.SPEED, 0) },
				new Pair[] { Pair.of(Attribute.GENERIC_MAX_HEALTH, 22.0) });

		new Kit("Enderman", new ItemStack[] { Metas.setName(Material.STONE_SWORD, "Arm"),
				new ItemStack(Material.ENDER_PEARL, 16) });

		new Kit("Commando", new ItemStack[] { Metas.setEnch(Material.BOW, "MG", Pair.of(Enchantment.ARROW_DAMAGE, 1)),
				new ItemStack(Material.LEATHER_HELMET, 1) });

		new Kit("Assault", Metas.setEnch(Material.BOW, "Gun", Pair.of(Enchantment.ARROW_DAMAGE, 1)),
				Pair.of(PotionEffectType.SPEED, 0));

		new Kit("WitherSkeleton", Material.STONE_SWORD, 22.0);

		new Kit("Rioter", new ItemStack(Material.BOW), Pair.of(PotionEffectType.SPEED, 1));

		new Kit("Pisto2", new ItemStack[] { Metas.setEnch(Material.BOW, Pair.of(Enchantment.ARROW_FIRE, 1)),
				new ItemStack(Material.TNT, 64) });

		new Kit("Crossbowman", Metas.setEnch(Material.BOW, Pair.of(Enchantment.ARROW_DAMAGE, 2)));
		//
		// Level 4
		//
		new Kit("Light", Metas.setEnch(Material.BOW, "Gun", Pair.of(Enchantment.ARROW_DAMAGE, 1)),
				Pair.of(PotionEffectType.SPEED, 1));

		new Kit("Gunner", new ItemStack[] { Metas.setEnch(Material.BOW, "MMG", Pair.of(Enchantment.ARROW_DAMAGE, 1)),
				new ItemStack(Material.TNT, 64) });

		new Kit("Eye", Metas.setEnch(Material.BOW, "Silencious", Pair.of(Enchantment.ARROW_DAMAGE, 1)),
				Pair.of(PotionEffectType.SPEED, 1));

		new Kit("Kommando",
				new ItemStack[] { Metas.setEnch(Material.BOW, "Gro Fling'", Pair.of(Enchantment.ARROW_DAMAGE, 2)),
						new ItemStack(Material.LEATHER_HELMET) });
		//
		// Level 5
		//
		new Kit("DoublePisto",
				new ItemStack[] { Metas.setEnch(Material.BOW, "Double Pisto", Pair.of(Enchantment.ARROW_DAMAGE, 2)),
						new ItemStack(Material.TNT, 64) });

		new Kit("Prisoner", (ItemStack[]) ArrayUtils.add(Mats.getFullSuit(Material.LEATHER),
				Metas.setName(Material.WOODEN_SWORD, "Stick")));

		new Kit("PY",
				new ItemStack[] { Metas.setName(Material.BOW, "Pistol"), Metas.setName(Material.IRON_HELMET, "Kepi") });

		new Kit("SniperAssault", Metas.setEnch(Material.BOW, "SMG", Pair.of(Enchantment.ARROW_DAMAGE, 3)),
				Pair.of(PotionEffectType.SPEED, 0));

		new Kit("AR", new ItemStack[] { Metas.setName(Material.STONE_SWORD, "Stick") },
				new Pair[] { Pair.of(PotionEffectType.SPEED, 0), Pair.of(PotionEffectType.ABSORPTION, 1) },
				new Pair[0]);

		new Kit("pH", new ItemStack[] { Metas.setName(Material.IRON_HELMET, "Limit"),
				Metas.setName(Material.BOW, "Graduator") });

		new Kit("Polizei",
				new ItemStack[] { new ItemStack(Material.STONE_SWORD), new ItemStack(Material.LEATHER_HELMET),
						Metas.setEnch(Material.BOW, Pair.of(Enchantment.ARROW_DAMAGE, 1)) });

		new Kit("Rat", Material.IRON_SWORD, new Pair[] { Pair.of(PotionEffectType.SPEED, 1) },
				new Pair[] { Pair.of(Attribute.GENERIC_MAX_HEALTH, 22.0) });
		//
		// Level 6
		//
		new Kit("Blaze",
				new ItemStack[] { Metas.setName(Material.BOW, "Stick"), Metas.setName(Material.WOODEN_SWORD, "Stick") },
				new Pair[] { Pair.of(PotionEffectType.SPEED, 1) },
				new Pair[] { Pair.of(Attribute.GENERIC_MAX_HEALTH, 24.0) });

		new Kit("Assassin", Metas.setName(Material.DIAMOND_SWORD, "Dagger"), Pair.of(PotionEffectType.SPEED, 0));

		new Kit("SY", Metas.setEnch(Material.BOW, "Sniper", Pair.of(Enchantment.ARROW_DAMAGE, 5)),
				Pair.of(PotionEffectType.SPEED, 0));

		new Kit("CostGuard", new ItemStack[] { new ItemStack(Material.LEATHER_HELMET), new ItemStack(Material.BOW) },
				new Pair[] { Pair.of(PotionEffectType.JUMP, 3) }, new Pair[0]);

		new Kit("AR2",
				(ItemStack[]) ArrayUtils.add(Metas.setNames(Mats.getFullSuit(Material.LEATHER), "Spatial Suit"),
						Metas.setName(Material.BOW, "Pistol")),
				new Pair[] { Pair.of(PotionEffectType.JUMP, 0) }, new Pair[0]);

		new Kit("Rebel",
				new ItemStack[] { Metas.setName(Material.BOW, "Contriband Gun"),
						Metas.setName(Material.LEATHER_HELMET, "Camo") },
				new Pair[] { Pair.of(PotionEffectType.INCREASE_DAMAGE, 0), Pair.of(PotionEffectType.SPEED, 0) },
				new Pair[0]);
		//
		// Level 7
		//
		new Kit("Rat2", new ItemStack[] { Metas.setName(Material.IRON_SWORD, "Fang") }, new Pair[0],
				new Pair[] { Pair.of(Attribute.GENERIC_MAX_HEALTH, 24.0) });

		new Kit("Milice", new ItemStack[] { new ItemStack(Material.BOW), Metas.setName(Material.WOODEN_SWORD, "Dagger"),
				Metas.setName(Material.IRON_HELMET, "Hat") });

		new Kit("Espion",
				new ItemStack[] { Metas.setEnch(Material.BOW, "Silencious", Pair.of(Enchantment.ARROW_DAMAGE, 1)) },
				new Pair[] { Pair.of(PotionEffectType.SPEED, 2), Pair.of(PotionEffectType.INVISIBILITY, 0),
						Pair.of(PotionEffectType.ABSORPTION, 1) },
				new Pair[0]);

		new Kit("AG",
				new ItemStack[] { new ItemStack(Material.BOW),
						Metas.setEnch(Material.IRON_HELMET, "Summit", Pair.of(Enchantment.PROTECTION_ENVIRONMENTAL, 1)),
						new ItemStack(Material.TNT, 64) });

		new Kit("Blaze2",
				new ItemStack[] { Metas.setEnch(Material.BOW, Pair.of(Enchantment.ARROW_FIRE, 1)),
						Metas.setEnch(Material.WOODEN_SWORD, Pair.of(Enchantment.FIRE_ASPECT, 1)) },
				new Pair[] { Pair.of(PotionEffectType.SPEED, 1) },
				new Pair[] { Pair.of(Attribute.GENERIC_MAX_HEALTH, 22.0) });
		//
		// Level 8
		//
		new Kit("SupportClone", new ItemStack[] { Metas.setName(Material.BOW, "LaserGun"),
				Metas.setName(Material.DIAMOND_HELMET, "Armor"), new ItemStack(Material.TNT, 64) });

		new Kit("Pilot",
				new ItemStack[] { Metas.setName(Material.WOODEN_SWORD, "Dagger"), new ItemStack(Material.IRON_HELMET) },
				new Pair[] { Pair.of(PotionEffectType.SPEED, 1) },
				new Pair[] { Pair.of(Attribute.GENERIC_MAX_HEALTH, 22.0) });

		new Kit("Sniper", new ItemStack[] { Metas.setEnch(Material.BOW, "Sniper", Pair.of(Enchantment.ARROW_DAMAGE, 5)),
				new ItemStack(Material.LEATHER_HELMET), new ItemStack(Material.LEATHER_CHESTPLATE) });

		new Kit("RemadeRobot", (ItemStack[]) ArrayUtils.addAll(
				Metas.setDamage(Metas.setEnch(Mats.getFullSuit(Material.DIAMOND), "Shield",
						Pair.of(Enchantment.PROTECTION_ENVIRONMENTAL, 10)), 10),
				new ItemStack[] { Metas.setEnch(Material.IRON_SWORD, "Drill", Pair.of(Enchantment.DAMAGE_ALL, 4)), Metas
						.setDamage(Metas.setEnch(Material.BOW, "Explosion", Pair.of(Enchantment.ARROW_FIRE, 1)), 1) }));
		//
		// Level 9
		//
		new Kit("AGY",
				new ItemStack[] { new ItemStack(Material.DIAMOND_CHESTPLATE),
						Metas.setEnch(Material.BOW, "Silencious", Pair.of(Enchantment.ARROW_DAMAGE, 1)),
						new ItemStack(Material.TNT, 64) });

		new Kit("Reporter", new ItemStack[] { Metas.setEnch(Material.BOW, Pair.of(Enchantment.ARROW_DAMAGE, 5)),
				new ItemStack(Material.IRON_HELMET) }, Pair.of(PotionEffectType.SLOW, 0));
		//
		// Level 10
		//
		new Kit("Swordsman", Metas.setEnch(Material.DIAMOND_SWORD, "Longsword", Pair.of(Enchantment.DAMAGE_ALL, 5)));

		new Kit("Swimmer",
				new ItemStack[] { Metas.setEnch(Material.BOW, "Harpoon", Pair.of(Enchantment.ARROW_DAMAGE, 1)),
						Metas.setName(Material.DIAMOND_HELMET, "Mask") });

		new Kit("Agent", new ItemStack[] { new ItemStack(Material.IRON_HELMET),
				Metas.setName(Material.LEATHER_CHESTPLATE, "Kevlar"), Metas.setName(Material.BOW, "Gun") });
		//
		// Level 11
		//
		new Kit("Soldier",
				(ItemStack[]) ArrayUtils.addAll(Mats.getFullSuit(Material.LEATHER),
						new ItemStack[] { Metas.setEnch(Material.BOW, "Sniper", Pair.of(Enchantment.ARROW_DAMAGE, 5)),
								Metas.setName(Material.WOODEN_SWORD, "Dagger") }));

		new Kit("Sailor",
				new ItemStack[] { new ItemStack(Material.IRON_HELMET),
						Metas.setEnch(Material.BOW, "Fishing Rod", Pair.of(Enchantment.ARROW_DAMAGE, 2)) },
				null, Pair.of(Attribute.GENERIC_MAX_HEALTH, 26.0));

		new Kit("Polizei2", new ItemStack[] {
				Metas.setEnch(Material.BOW, "Pistol", Pair.of(Enchantment.ARROW_DAMAGE, 2)),
				Metas.setEnch(Material.IRON_HELMET, "Kepi", Pair.of(Enchantment.PROTECTION_ENVIRONMENTAL, 3)) });

		new Kit("Soldier2",
				(ItemStack[]) ArrayUtils.addAll(Mats.getFullSuit(Material.LEATHER),
						new ItemStack[] { Metas.setEnch(Material.BOW, Pair.of(Enchantment.ARROW_DAMAGE, 5)),
								new ItemStack(Material.WOODEN_SWORD) }));

		new Kit("Officer",
				(ItemStack[]) ArrayUtils.addAll(Mats.getFullSuit(Material.LEATHER),
						new ItemStack[] { Metas.setEnch(Material.BOW, Pair.of(Enchantment.ARROW_DAMAGE, 2)),
								new ItemStack(Material.DIAMOND_SWORD) }));
		//
		// Level12
		//
		new Kit("Officer2",
				(ItemStack[]) ArrayUtils.addAll(Mats.getFullSuit(Material.LEATHER),
						new ItemStack[] { Metas.setEnch(Material.BOW, "Gun", Pair.of(Enchantment.ARROW_DAMAGE, 2)),
								new ItemStack(Material.DIAMOND_SWORD) }));

		new Kit("Barbarian", Metas.setEnch(Material.IRON_SWORD, "Barbarian Sword", Pair.of(Enchantment.DAMAGE_ALL, 6)),
				Pair.of(PotionEffectType.INCREASE_DAMAGE, 1));

		new Kit("Guard",
				new ItemStack[] { Metas.setEnch(Material.IRON_SWORD, "Nightstick", Pair.of(Enchantment.DAMAGE_ALL, 1)),
						Metas.setEnch(Material.IRON_HELMET, Pair.of(Enchantment.PROTECTION_ENVIRONMENTAL, 1)) },
				Pair.of(PotionEffectType.INCREASE_DAMAGE, 1));
		//
		// Level 13
		//
		new Kit("Scientist",
				new ItemStack[] { Metas.setName(Material.WOODEN_SWORD, "Clip"),
						Metas.setName(Material.IRON_HELMET, "Microscope") },
				new Pair[] { Pair.of(PotionEffectType.ABSORPTION, 2), Pair.of(PotionEffectType.SPEED, 1) },
				new Pair[] { Pair.of(Attribute.GENERIC_MAX_HEALTH, 26.0) });

		new Kit("Jedi", Metas.setEnch(Material.DIAMOND_SWORD, "Lightsaber", Pair.of(Enchantment.DAMAGE_ALL, 5)),
				new Pair[] { Pair.of(PotionEffectType.ABSORPTION, 2), Pair.of(PotionEffectType.JUMP, 1) });

		new Kit("CloneGeneral",
				new ItemStack[] { new ItemStack(Material.WOODEN_SWORD),
						Metas.setName(Material.IRON_CHESTPLATE, "Clone Armor"),
						Metas.setName(Material.IRON_LEGGINGS, "Clone Amor"),
						Metas.setName(Material.IRON_BOOTS, "Clone Armor") });
		//
		// Level 15
		//
		new Kit("Agien", new ItemStack[] { new ItemStack(Material.IRON_HELMET),
				Metas.setName(Material.STONE_SWORD, "Battle Sword"), new ItemStack(Material.LEATHER_CHESTPLATE) },
				new Pair[] { Pair.of(PotionEffectType.INCREASE_DAMAGE, 1), Pair.of(PotionEffectType.NIGHT_VISION, 0),
						Pair.of(PotionEffectType.SLOW, 0) });
		//
		// Level 16
		//
		new Kit("King",
				new ItemStack[] {
						Metas.setEnch(Material.GOLDEN_HELMET, "Crown",
								Pair.of(Enchantment.PROTECTION_ENVIRONMENTAL, 4)),
						Metas.setEnch(Material.GOLDEN_SWORD, "King Sword", Pair.of(Enchantment.DAMAGE_ALL, 6)) });
		//
		// Level 17
		//
		new Kit("Clone", (ItemStack[]) ArrayUtils.add(Metas.setNames(Mats.getFullSuit(Material.IRON_INGOT), "Armor"),
				Metas.setName(Material.BOW, "Laser Gun")));
		//
		// Level 18
		//
		new Kit("AAN",
				new ItemStack[] { Metas.setName(Material.IRON_HELMET, "Spatial Heltmet"),
						Metas.setEnch(Material.BOW, "Pistol", Pair.of(Enchantment.ARROW_DAMAGE, 5)) },
				Pair.of(PotionEffectType.JUMP, 3));

		new Kit("SupportLaserClone",
				(ItemStack[]) ArrayUtils.add(Metas.setNames(Mats.getFullSuit(Material.IRON_INGOT), "Armor"),
						Metas.setEnch(Material.BOW, Pair.of(Enchantment.ARROW_FIRE, 1))));
		//
		// Level 19
		//
		// MAGES.
		new Kit("Mage",
				new ItemStack[] { Metas.setName(Material.DIAMOND_CHESTPLATE, "Magic suit"),
						Metas.setName(Material.DIAMOND_SWORD, "Magic wand"),
						Metas.setName(Material.LEATHER_LEGGINGS, "Magic suit"),
						Metas.setName(Material.LEATHER_BOOTS, "Magic suit"),
						Metas.setEnch(Material.BOW, "Magic wand", Pair.of(Enchantment.ARROW_DAMAGE, 5)) });

		new Kit("CloneLambda",
				(ItemStack[]) ArrayUtils.add(Metas.setNames(Mats.getFullSuit(Material.IRON_INGOT), "Clone Armor"),
						Metas.setEnch(Material.BOW, "Laser Gun", Pair.of(Enchantment.ARROW_DAMAGE, 2))));

		new Kit("CloneV2",
				(ItemStack[]) ArrayUtils.addAll(Metas.setNames(Mats.getFullSuit(Material.IRON_INGOT), "Clone Armor"),
						new ItemStack[] { Metas.setName(Material.BOW, "Laser Gun"), new ItemStack(Material.TNT, 64),
								new ItemStack(Material.REDSTONE_TORCH, 64) }));
		//
		// Level 20
		//
		new Kit("Droid",
				(ItemStack[]) ArrayUtils.add(Metas.setNames(Mats.getFullSuit(Material.IRON_INGOT), "Droid Corps"),
						Metas.setEnch(Material.BOW, "Laser Gun", Pair.of(Enchantment.ARROW_DAMAGE, 2))));
		//
		// Level 21
		//
		new Kit("Gendre",
				new ItemStack[] { new ItemStack(Material.BOW),
						Metas.setEnch(Material.STONE_SWORD, Pair.of(Enchantment.DAMAGE_ALL, 5)),
						Metas.setEnch(Material.CHAINMAIL_HELMET, Pair.of(Enchantment.PROTECTION_ENVIRONMENTAL, 1)),
						Metas.setEnch(Material.CHAINMAIL_CHESTPLATE,
								Pair.of(Enchantment.PROTECTION_ENVIRONMENTAL, 4)) },
				new Pair[] { Pair.of(PotionEffectType.JUMP, 3), Pair.of(PotionEffectType.SPEED, 0),
						Pair.of(PotionEffectType.ABSORPTION, 0) });

		new Kit("Jedi2",
				new ItemStack[] {
						Metas.setEnch(Material.DIAMOND_SWORD, "Green Light Saber", Pair.of(Enchantment.DAMAGE_ALL, 10)),
						Metas.setName(Material.IRON_HELMET, "Ice Hat"),
						Metas.setName(Material.LEATHER_CHESTPLATE, "Jedi Tunic"),
						Metas.setName(Material.LEATHER_LEGGINGS, "Jedi Tunic") });
		//
		// Level22
		//
		new Kit("SubSergeantClone",
				(ItemStack[]) ArrayUtils.add(
						Metas.setEnch(Mats.getFullSuit(Material.IRON_INGOT), "Clone Armor",
								Pair.of(Enchantment.PROTECTION_ENVIRONMENTAL, 1)),
						Metas.setEnch(Material.BOW, "Laser Gun", Pair.of(Enchantment.ARROW_DAMAGE, 4))));
		//
		// Level23
		//
		new Kit("Agent", new ItemStack[] {
				Metas.setEnch(Material.GOLDEN_HELMET, "Observer", Pair.of(Enchantment.PROTECTION_ENVIRONMENTAL, 8)),
				Metas.setEnch(Material.GOLDEN_SWORD, "Knife", Pair.of(Enchantment.PROTECTION_ENVIRONMENTAL, 8)) });
		//
		// Level24
		//
		new Kit("SergeantClone",
				(ItemStack[]) ArrayUtils.add(Metas.setNames(Mats.getFullSuit(Material.IRON_INGOT), "Clone Armor"),
						Metas.setEnch(Material.DIAMOND_SWORD, "Commander Sword", Pair.of(Enchantment.DAMAGE_ALL, 3))));

		new Kit("DroidEye",
				new ItemStack[] {
						Metas.setEnch(Material.IRON_HELMET, "DroidHead",
								Pair.of(Enchantment.PROTECTION_ENVIRONMENTAL, 1)),
						Metas.setName(Material.IRON_CHESTPLATE, "Droid Armor"),
						Metas.setName(Material.DIAMOND_LEGGINGS, "Droid Armor"),
						Metas.setName(Material.DIAMOND_BOOTS, "Droid Amor"),
						Metas.setName(Material.DIAMOND_SWORD, "Commander Sword") });
		//
		// Level26
		//
		new Kit("Zombie", (ItemStack[]) ArrayUtils.add(Metas.setNames(Mats.getFullSuit(Material.DIAMOND), " "),
				Metas.setName(Material.DIAMOND_SWORD, " ")));

		new Kit("Bodyguard",
				new ItemStack[] {
						Metas.setEnch(Material.DIAMOND_CHESTPLATE, "Kevlar",
								Pair.of(Enchantment.PROTECTION_ENVIRONMENTAL, 4)),
						Metas.setEnch(Material.BOW, "...", Pair.of(Enchantment.ARROW_DAMAGE, 5)),
						Metas.setEnch(Material.DIAMOND_SWORD, "...", Pair.of(Enchantment.DAMAGE_ALL, 5)) });
		//
		// Level27
		//
		new Kit("DaBaDee",
				(ItemStack[]) ArrayUtils.addAll(
						Metas.setNames(Mats.getFullSuit(Material.DIAMOND), "I'm Blue Dabadee Dabada"),
						new ItemStack[] { Metas.setName(Material.DIAMOND_SWORD, "I'm Blue"),
								Metas.setName(Material.BOW, "Dabadee") }),
				new Pair[] { Pair.of(PotionEffectType.NIGHT_VISION, 0), Pair.of(PotionEffectType.SPEED, 0) });
		//
		// Level43
		//
		new Kit("GoldenVIP", (ItemStack[]) ArrayUtils.addAll(
				Metas.setEnch(Mats.getFullSuit(Material.GOLD_INGOT), "VIP",
						Pair.of(Enchantment.PROTECTION_ENVIRONMENTAL, 6)),
				new ItemStack[] { Metas.setEnch(Material.GOLDEN_SWORD, "VIP", Pair.of(Enchantment.DAMAGE_ALL, 2)),
						new ItemStack(Material.BOW) }),
				new Pair[] { Pair.of(PotionEffectType.FIRE_RESISTANCE, 0), Pair.of(PotionEffectType.ABSORPTION, 3) });
		//
		// Level53
		//
		new Kit("Wither",
				(ItemStack[]) ArrayUtils.addAll(Mats.getFullSuit(Material.DIAMOND),
						new ItemStack[] {
								Metas.setEnch(Material.BOW, "Wither Heads", Pair.of(Enchantment.ARROW_DAMAGE, 5)),
								new ItemStack(Material.ENCHANTED_GOLDEN_APPLE, 1) }));
		//
		// Level54
		//
		new Kit("Redstoner",
				(ItemStack[]) ArrayUtils
						.addAll(Metas.setEnch(Mats.getFullSuit(Material.LEATHER), Pair.of(Enchantment.THORNS, 1)),
								new ItemStack[] {
										Metas.setAttr(Material.IRON_SWORD, "Redstone",
												Pair.of(Attribute.GENERIC_ATTACK_DAMAGE,
														new AttributeModifier("generic.attackDamage", 2.0,
																Operation.MULTIPLY_SCALAR_1))),
										new ItemStack(Material.BOW) }),
				Pair.of(PotionEffectType.INCREASE_DAMAGE, 0));
		//
		// Level62
		//
		new Kit("Nature",
				(ItemStack[]) ArrayUtils.addAll(
						Metas.setEnchs(Mats.getFullSuit(Material.LEATHER), "Nature Armor",
								new Pair[] { Pair.of(Enchantment.THORNS, 5),
										Pair.of(Enchantment.PROTECTION_ENVIRONMENTAL, 5) }),
						new ItemStack[] {
								Metas.setMetas(Material.WOODEN_SWORD, "Nature", Pair.of(Enchantment.DAMAGE_ALL, 10),
										Pair.of(Attribute.GENERIC_ATTACK_DAMAGE,
												new AttributeModifier("generic.attackDamage", 2.0,
														Operation.MULTIPLY_SCALAR_1))),
								Metas.setName(Material.BOW, "YOU") }),
				new Pair[] { Pair.of(PotionEffectType.HUNGER, 0) },
				new Pair[] { Pair.of(Attribute.GENERIC_MAX_HEALTH, 18.0) });
		//
		// Level67
		//
		new Kit("EnderDragon", (ItemStack[]) ArrayUtils.addAll(
				Metas.setEnch(Mats.getFullSuit(Material.DIAMOND), "GG",
						Pair.of(Enchantment.PROTECTION_ENVIRONMENTAL, 5)),
				new ItemStack[] { Metas.setEnch(Material.DIAMOND_SWORD, "Void", Pair.of(Enchantment.DAMAGE_ALL, 5)),
						Metas.setEnch(Material.BOW, "Void", Pair.of(Enchantment.ARROW_DAMAGE, 5)),
						new ItemStack(Material.ENCHANTED_GOLDEN_APPLE, 1) }));
		//
		//
		//
		//
		//
		new Kit("BuildUhc",
				new ItemStack[] { Metas.setEnch(Material.DIAMOND_SWORD, Pair.of(Enchantment.DAMAGE_ALL, 4)),
						Metas.setEnch(Material.BOW, Pair.of(Enchantment.ARROW_DAMAGE, 3)),
						Metas.setEnch(Material.DIAMOND_HELMET, Pair.of(Enchantment.PROTECTION_PROJECTILE, 2)),
						Metas.setEnch(Material.DIAMOND_CHESTPLATE, Pair.of(Enchantment.PROTECTION_ENVIRONMENTAL, 2)),
						Metas.setEnch(Material.DIAMOND_LEGGINGS, Pair.of(Enchantment.PROTECTION_PROJECTILE, 2)),
						Metas.setEnch(Material.DIAMOND_BOOTS, Pair.of(Enchantment.PROTECTION_ENVIRONMENTAL, 2)),
						new ItemStack(Material.GOLDEN_APPLE, 14), new ItemStack(Material.OAK_PLANKS, 128),
						new ItemStack(Material.COBBLESTONE, 128),
						Metas.setEnch(Material.DIAMOND_PICKAXE, Pair.of(Enchantment.DIG_SPEED, 5)),
						new ItemStack(Material.DIAMOND_AXE),
						Metas.setEnch(Material.FISHING_ROD, Pair.of(Enchantment.DURABILITY, 10)),
						new ItemStack(Material.WATER_BUCKET, 2), new ItemStack(Material.LAVA_BUCKET, 2) });

		new Kit("2B2T",
				new ItemStack[] {
						Metas.setEnchs(new ItemStack(Material.DIAMOND_HELMET),
								new Pair[] { Pair.of(Enchantment.PROTECTION_ENVIRONMENTAL, 4),
										Pair.of(Enchantment.WATER_WORKER, 1), Pair.of(Enchantment.VANISHING_CURSE, 1),
										Pair.of(Enchantment.MENDING, 1), Pair.of(Enchantment.DURABILITY, 3),
										Pair.of(Enchantment.THORNS, 3), Pair.of(Enchantment.OXYGEN, 3) }),
						Metas.setEnchs(new ItemStack(Material.DIAMOND_CHESTPLATE),
								new Pair[] { Pair.of(Enchantment.PROTECTION_ENVIRONMENTAL, 4),
										Pair.of(Enchantment.VANISHING_CURSE, 1), Pair.of(Enchantment.MENDING, 1),
										Pair.of(Enchantment.DURABILITY, 3), Pair.of(Enchantment.THORNS, 3) }),
						Metas.setEnchs(new ItemStack(Material.DIAMOND_LEGGINGS),
								new Pair[] { Pair.of(Enchantment.PROTECTION_ENVIRONMENTAL, 4),
										Pair.of(Enchantment.VANISHING_CURSE, 1), Pair.of(Enchantment.MENDING, 1),
										Pair.of(Enchantment.DURABILITY, 3), Pair.of(Enchantment.THORNS, 3) }),
						Metas.setEnchs(new ItemStack(Material.DIAMOND_BOOTS),
								new Pair[] { Pair.of(Enchantment.PROTECTION_ENVIRONMENTAL, 4),
										Pair.of(Enchantment.VANISHING_CURSE, 1), Pair.of(Enchantment.MENDING, 1),
										Pair.of(Enchantment.DURABILITY, 3), Pair.of(Enchantment.THORNS, 3),
										Pair.of(Enchantment.PROTECTION_FALL, 4), Pair.of(Enchantment.FROST_WALKER, 2),
										Pair.of(Enchantment.DEPTH_STRIDER, 3) }),
						Metas.setEnchs(Material.BOW,
								new Pair[] { Pair.of(Enchantment.ARROW_DAMAGE, 5), Pair.of(Enchantment.ARROW_FIRE, 1),
										Pair.of(Enchantment.MENDING, 1), Pair.of(Enchantment.ARROW_KNOCKBACK, 2),
										Pair.of(Enchantment.DURABILITY, 3), Pair.of(Enchantment.VANISHING_CURSE, 1) }),
						new ItemStack(Material.END_CRYSTAL, 128), new ItemStack(Material.OBSIDIAN, 128),
						new ItemStack(Material.ENCHANTED_GOLDEN_APPLE, 64),
						new ItemStack(Material.EXPERIENCE_BOTTLE, 256),
						Metas.setEnchs(Material.DIAMOND_PICKAXE,
								new Pair[] { Pair.of(Enchantment.DURABILITY, 3), Pair.of(Enchantment.DIG_SPEED, 5),
										Pair.of(Enchantment.MENDING, 1), Pair.of(Enchantment.LOOT_BONUS_BLOCKS, 3),
										Pair.of(Enchantment.VANISHING_CURSE, 1) }),
						new ItemStack(Material.CHORUS_FRUIT, 64) });

		new Kit("Sword",
				Metas.setEnchs(Material.DIAMOND_SWORD,
						new Pair[] { Pair.of(Enchantment.DAMAGE_ALL, 5), Pair.of(Enchantment.DURABILITY, 3),
								Pair.of(Enchantment.SWEEPING_EDGE, 3), Pair.of(Enchantment.FIRE_ASPECT, 2),
								Pair.of(Enchantment.LOOT_BONUS_MOBS, 3), Pair.of(Enchantment.MENDING, 1) }));
	}
}
