package fr.swarmyyyy.pvpkm.core;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.LinkedHashSet;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeModifier;
import org.bukkit.craftbukkit.libs.org.apache.commons.lang3.tuple.Pair;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.potion.PotionEffectType;

import fr.swarmyyyy.utilities.Ut;
import fr.swarmyyyy.utilities.mc.Base;
import fr.swarmyyyy.utilities.mc.McToS;

public class Kit {

	private static LinkedHashSet<Kit> kits = new LinkedHashSet<Kit>();

	public static LinkedHashSet<Kit> getKits() {
		return kits;
	}

	/**
	 * Returns the Kit if it exists, else null
	 * 
	 * @param name The name of the Kit
	 * @return the Kit or null
	 */
	public static Kit get(String name) {
		for (Kit kit : kits) {
			if (kit.name.equalsIgnoreCase(name)) {
				return kit;
			}
		}
		return null;
	}

	private static byte arrowBase = 8;

	public static byte getArrowBase() {
		return arrowBase;
	}

	public static void setArrowBase(byte arrowBase) {
		Kit.arrowBase = arrowBase;
	}

	private static byte foodBase = 2;

	public static byte getFoodBase() {
		return foodBase;
	}

	public static void setFoodBase(byte foodBase) {
		Kit.foodBase = foodBase;
	}

	private static boolean isArrowDropOnNotBowKits = true;

	public static boolean isArrowDropOnNotBowKits() {
		return isArrowDropOnNotBowKits;
	}

	public static void setArrowDropOnNotBowKits(boolean isArrowDropOnNotBowKits) {
		Kit.isArrowDropOnNotBowKits = isArrowDropOnNotBowKits;
	}

	private static Hashtable<Material, Double> matValue = new Hashtable<Material, Double>() {
		private static final long serialVersionUID = -7074004897306424763L;
		{
			put(Material.GOLDEN_SWORD, -1.0);
			put(Material.STONE_SWORD, 2.0);
			put(Material.IRON_SWORD, 3.0);
			put(Material.DIAMOND_SWORD, 4.0);

			put(Material.GOLDEN_BOOTS, 2.0);
			put(Material.GOLDEN_CHESTPLATE, 2.0);
			put(Material.GOLDEN_HELMET, 2.0);
			put(Material.GOLDEN_LEGGINGS, 2.0);

			put(Material.IRON_CHESTPLATE, 3.0);
			put(Material.IRON_HELMET, 3.0);
			put(Material.IRON_LEGGINGS, 3.0);
			put(Material.IRON_BOOTS, 3.0);

			put(Material.CHAINMAIL_BOOTS, 4.0);
			put(Material.CHAINMAIL_CHESTPLATE, 4.0);
			put(Material.CHAINMAIL_HELMET, 4.0);
			put(Material.CHAINMAIL_LEGGINGS, 4.0);

			put(Material.DIAMOND_BOOTS, 5.0);
			put(Material.DIAMOND_CHESTPLATE, 5.0);
			put(Material.DIAMOND_HELMET, 5.0);
			put(Material.DIAMOND_LEGGINGS, 5.0);

			put(Material.GOLDEN_AXE, -1.0);
			put(Material.STONE_AXE, 2.0);
			put(Material.IRON_AXE, 3.0);
			put(Material.DIAMOND_AXE, 4.0);

			put(Material.GOLDEN_PICKAXE, -6.0);
			put(Material.WOODEN_PICKAXE, -4.0);
			put(Material.STONE_PICKAXE, -3.0);
			put(Material.IRON_PICKAXE, -2.0);
			put(Material.DIAMOND_PICKAXE, -1.0);

			put(Material.ENCHANTED_GOLDEN_APPLE, 64.0);
			put(Material.GOLDEN_APPLE, 2.0);
			put(Material.COBBLESTONE, 0.125);
			put(Material.OAK_PLANKS, 0.125);
			put(Material.END_CRYSTAL, 16.0);
			put(Material.EXPERIENCE_BOTTLE, 0.03);

			put(Material.LAVA_BUCKET, 0.125);
			put(Material.WATER_BUCKET, 0.125);

		}
	};

	private static Hashtable<Attribute, Double> attrMult = new Hashtable<Attribute, Double>() {
		private static final long serialVersionUID = 6458739757469050157L;
		{
			put(Attribute.GENERIC_MAX_HEALTH, 0.5);
		}
	};

	private static Hashtable<PotionEffectType, Double> effMult = new Hashtable<PotionEffectType, Double>() {
		private static final long serialVersionUID = -3767394109646514693L;
		{
			put(PotionEffectType.ABSORPTION, 0.5);
			put(PotionEffectType.JUMP, 0.25);
			put(PotionEffectType.SLOW, -1.0);
			put(PotionEffectType.INCREASE_DAMAGE, 2.0);
			put(PotionEffectType.HUNGER, -1.0);
		}
	};

	private static Hashtable<Enchantment, Double> enchMult = new Hashtable<Enchantment, Double>() {
		private static final long serialVersionUID = -8790177126226737183L;
		{
			put(Enchantment.ARROW_DAMAGE, 2.0);
			put(Enchantment.FIRE_ASPECT, 2.0);
			put(Enchantment.DIG_SPEED, 0.2);
			put(Enchantment.DURABILITY, 0.4);
			put(Enchantment.VANISHING_CURSE, -1.0);
		}
	};

	private String name;

	public String getName() {
		return name;
	}

	private int level;

	public int getLevel() {
		return level;
	}

	private ItemStack[] items;

	public ItemStack[] getItems() {
		return items;
	}

	private LinkedHashSet<Pair<PotionEffectType, Integer>> effects;

	public LinkedHashSet<Pair<PotionEffectType, Integer>> getEffects() {
		return effects;
	}

	private LinkedHashSet<Pair<Attribute, Double>> playerAttrs;

	public LinkedHashSet<Pair<Attribute, Double>> getPlayerAttrs() {
		return playerAttrs;
	}

	/**
	 * TODO In case there is a conflict in the kits, Override this
	 */
	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return super.equals(obj);
	}

	@SuppressWarnings("unchecked")
	public Kit(String name, Material mat) {
		setKit(name, new ItemStack[] { new ItemStack(mat) }, new Pair[0], new Pair[0]);
	};

	@SuppressWarnings("unchecked")
	public Kit(String name, Material mat, double maxHealth) {
		setKit(name, new ItemStack[] { new ItemStack(mat) }, new Pair[0], new Pair[0]);
	};

	@SuppressWarnings("unchecked")
	public Kit(String name, Material mat, Pair<PotionEffectType, Integer> effectNlevel,
			Pair<Attribute, Double> playerAttrs) {
		setKit(name, new ItemStack[] { new ItemStack(mat) }, new Pair[] { effectNlevel }, new Pair[] { playerAttrs });
	}

	@SuppressWarnings("unchecked")
	public Kit(String name, ItemStack item, Pair<PotionEffectType, Integer> effectNlevel) {
		setKit(name, new ItemStack[] { item }, new Pair[] { effectNlevel }, new Pair[0]);
	}

	@SuppressWarnings("unchecked")
	public Kit(String name, ItemStack[] items, Pair<PotionEffectType, Integer>[] effectsNlevels) {
		setKit(name, items, effectsNlevels, new Pair[0]);
	}

	@SuppressWarnings("unchecked")
	public Kit(String name, ItemStack[] items, Pair<PotionEffectType, Integer> effectNlevel) {
		setKit(name, items, new Pair[] { effectNlevel }, new Pair[0]);
	}

	@SuppressWarnings("unchecked")
	public Kit(String name, ItemStack item, Pair<PotionEffectType, Integer> effectsNlevels[]) {
		setKit(name, new ItemStack[] { item }, effectsNlevels, new Pair[0]);
	}

	@SuppressWarnings("unchecked")
	public Kit(String name, ItemStack item, Pair<PotionEffectType, Integer> effectNlevel,
			Pair<Attribute, Double> playerAttrs) {
		setKit(name, new ItemStack[] { item }, new Pair[] { effectNlevel }, new Pair[] { playerAttrs });
	}

	@SuppressWarnings("unchecked")
	public Kit(String name, ItemStack[] items, Pair<PotionEffectType, Integer> effectNlevel,
			Pair<Attribute, Double> playerAttrs) {
		setKit(name, items, new Pair[] { effectNlevel }, new Pair[] { playerAttrs });
	}

	@SuppressWarnings("unchecked")
	public Kit(String name, ItemStack[] items) {
		setKit(name, items, new Pair[0], new Pair[0]);
	}

	@SuppressWarnings("unchecked")
	public Kit(String name, ItemStack item) {
		setKit(name, new ItemStack[] { item }, new Pair[0], new Pair[0]);
	}

	public Kit(String name, Material mat, Pair<PotionEffectType, Integer>[] effectsNlevels,
			Pair<Attribute, Double>[] playerAttrs) {
		setKit(name, new ItemStack[] { new ItemStack(mat) }, effectsNlevels, playerAttrs);
	}

	public Kit(String name, ItemStack item, Pair<PotionEffectType, Integer>[] effectsNlevels,
			Pair<Attribute, Double>[] playerAttrs) {
		setKit(name, new ItemStack[] { item }, effectsNlevels, playerAttrs);
	}

	public Kit(String name, ItemStack[] items, Pair<PotionEffectType, Integer>[] effectsNlevels,
			Pair<Attribute, Double>[] playerAttrs) {
		setKit(name, items, effectsNlevels, playerAttrs);
	}

	private void setKit(String name, ItemStack[] items, Pair<PotionEffectType, Integer>[] effectsNlevels,
			Pair<Attribute, Double>[] playerAttrs) {
		// Bukkit.getLogger().info(name);
		// Counting the level of the kit
		int level = countLevel(items, effectsNlevels, playerAttrs);
		//
		boolean hasBow = false;
		// Bukkit.getLogger().info("Kit :"+name+" "+hasBow);
		List<ItemStack> setItems = new ArrayList<ItemStack>();
		for (int i = 0; i < items.length; i++) {
			setItems.add(items[i]);
			// Bukkit.getLogger().info(""+items[i].getType());
			switch (items[i].getType()) {
			case BOW:
				hasBow = true;
				break;
			case TNT:
				setItems.add(new ItemStack(Material.REDSTONE_TORCH, 64));
				break;
			default:
				break;
			}
		}
		if (hasBow || isArrowDropOnNotBowKits) {
			setItems.add(new ItemStack(Material.ARROW, level * arrowBase));
		}
		setItems.add(new ItemStack(Material.COOKED_BEEF, level * foodBase));
		// Adding
		// Theses
		this.name = name.toLowerCase();
		this.level = level;
		this.items = setItems.toArray(new ItemStack[setItems.size()]);
		this.effects = Ut.arrayAsLinkedHashSet(effectsNlevels);
		try {
			this.playerAttrs = Ut.arrayAsLinkedHashSet(playerAttrs);
		} catch (NullPointerException ex) {
			this.playerAttrs = new LinkedHashSet<Pair<Attribute, Double>>();
		}
		kits.add(this);
	}

	private static int countLevel(ItemStack[] items, Pair<PotionEffectType, Integer>[] effectsNlevels,
			Pair<Attribute, Double>[] playerAttrs) {
		int level = 0;
		// Items value
		ArrayList<Double> itemsVal = new ArrayList<Double>(items.length);
		for (ItemStack item : items) {
			double itemVal = 0.0;
			// Item value
			double value = 1.0;
			try {
				value = matValue.get(item.getType()) * Double.valueOf(item.getAmount())
						/ Double.valueOf(item.getMaxStackSize());
			} catch (NullPointerException ex) {
			}
			itemVal += value;
			// Item Enchs value
			int i = 0;
			for (int enchl : item.getEnchantments().values()) {
				double mult = 1.0;
				try {
					mult = enchMult.get(item.getEnchantments().keySet().toArray()[i]);
				} catch (NullPointerException ex) {
				}
				itemVal += mult * enchl;
				i++;
			}
			// Item Attributes value
			try {
				i = 0;
				for (AttributeModifier attrM : item.getItemMeta().getAttributeModifiers().values()) {
					value = 0.0;
					double mult = 1.0;
					try {
						mult = attrMult.get(item.getItemMeta().getAttributeModifiers().keySet().toArray()[i]);
					} catch (NullPointerException ex) {
					}
					switch (attrM.getOperation()) {
					case ADD_NUMBER:
						value += attrM.getAmount() * mult;
						break;
					case ADD_SCALAR:
						value += attrM.getAmount() * mult;
						break;
					case MULTIPLY_SCALAR_1:
						value *= attrM.getAmount() * mult;
						break;
					}
					itemVal += value;
					i++;
				}
			} catch (NullPointerException ex) {
			}
			// Balancing in accordance with the Durability the item have
			try {
				/*
				 * Bukkit.getLogger().info("itemVal: " + itemVal + " damage: " +
				 * imdmg.getDamage() + " item: " + item.getType() + " maxdur: " +
				 * item.getType().getMaxDurability());
				 */
				Damageable imdmg = (Damageable) item.getItemMeta();
				double duraScaledVal = (Double.valueOf(item.getType().getMaxDurability())
						- Double.valueOf(imdmg.getDamage())) / Double.valueOf(item.getType().getMaxDurability());
				if (Double.isNaN(duraScaledVal)) {
					duraScaledVal = 1.0;
				}
				itemVal *= duraScaledVal;
			} catch (ArithmeticException ex) {
			}
			// Bukkit.getLogger().info("itemVal : " + itemVal);
			itemsVal.add(itemVal);
		}
		// Adding all the values of the Items to the level
		double total = 0.0;
		for (double val : itemsVal) {
			total += val;
		}
		level += total;
		// Effects value
		ArrayList<Double> effectsVal = new ArrayList<Double>(effectsNlevels.length);
		for (Pair<PotionEffectType, Integer> effect : effectsNlevels) {
			// <SPEED,0> is equivalent as Speed 1 in the game, and <SPEED,1> = Speed 2
			double mult = 1.0;
			try {
				mult = effMult.get(effect.getLeft());
			} catch (NullPointerException ex) {
			}
			try {
				level += (effect.getRight() + 1) * mult;
			} catch (NullPointerException ex) {
			}
		}
		// Adding all the values of the Effects to the level
		total = 0.0;
		for (double val : effectsVal) {
			total += val;
		}
		level += total;
		// Player Attributes values
		ArrayList<Double> attrsVal = new ArrayList<Double>(effectsNlevels.length);
		for (Pair<Attribute, Double> attrM : playerAttrs) {
			try {
				double mult = 1.0;
				try {
					mult = attrMult.get(attrM.getLeft());
				} catch (NullPointerException ex) {
				}
				double attrBase = 0.0;
				try {
					attrBase = Base.getAttrBase().get(attrM.getLeft());
				} catch (NullPointerException ex) {
				}
				// Bukkit.getLogger().info(""+attrM.getRight());
				level += attrM.getRight() - attrBase * mult;
			} catch (NullPointerException ex) {
			}
		}
		// Adding all the values of the Attributes to the level
		total = 0.0;
		for (double val : attrsVal) {
			total += val;
		}
		level += total;
		return level;
	}

	public static String kitCompToString(Kit kit) {

		return String.format("%s\n%s\n%s\n%s\n%s\n%s\n", McToS.itemsToString(kit.getItems()) == "" ? "" : "Items",
				McToS.itemsToString(kit.getItems()), McToS.effToString(kit.getEffects()) == "" ? "" : "Effects",
				McToS.effToString(kit.getEffects()),
				McToS.playerAttrsToString(kit.getPlayerAttrs()) == "" ? "" : "Attributes",
				McToS.playerAttrsToString(kit.getPlayerAttrs()));
	}

}
