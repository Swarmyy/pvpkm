package fr.swarmyyyy;

import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;

public class MyPluginListeners implements Listener {

	private static boolean LastResult = false;

	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		new SwPlayer(e.getPlayer());
		LastResult = SwPlayer.isLastResult();
	}

	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent e) {
		Player p = e.getEntity();
		// Getting you out of the game when you are dead
		SwPlayer.getSPlayer(p).setInGame(false);
		// Resetting health
		p.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(20);
		// Adding a level to the player killer
		if (p.getKiller() instanceof Player) {
			Player k = p.getKiller();
			if (SwPlayer.getSPlayer(k).getLevel() >= Byte.MAX_VALUE) {
				k.sendMessage("You have achieved max level, well played !");
			} else {
				SwPlayer.getSPlayer(p.getKiller())
						.setLevel((byte) (SwPlayer.getSPlayer(p.getKiller()).getLevel() + 1));
			}
		}
	}

	@EventHandler
	public void onEntityDeath(EntityDeathEvent e) {

	}

	public static boolean isLastResult() {
		return LastResult;
	}

}
